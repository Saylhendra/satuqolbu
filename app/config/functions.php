<?php
use Phalcon\Logger\Adapter\File as FileAdapter;
class Safe implements ArrayAccess
{
	// a Safe instance represeting used as a null result wrapper
	private static $_none = null;

	// Factory method to allow chaining
	static function make($obj = null)
	{
		return $obj !== null
			? new self($obj)
			: self::none();
	}

	// returns a singleton Safe instance representing an empty object
	// (used when a method wrapped by Safe returns null)
	static function none()
	{
		return self::$_none
			? self::$_none
			: self::$_none = new self;
	}

	// wrapped object reference
	private $_obj = null;

	function __construct($obj = null)		{ $this->_obj = $obj; }

	// dereference the wrapped instance
	function safe_get($default = null)		{ return $this->_obj !== null ? $this->_obj : $default; }

	// does this Safe instance wrap anything?
	function isNone()						{ return $this === self::none(); }

	// ArrayAccess interface implementation
	// to allow array access on wrapped instance fields
	function offsetExists($offset)			{ return is_array($this->_obj) && key_exists($offset, $this->_obj); }

	function offsetGet($offset)
	{
		return $this->offsetExists($offset)
			? self::make($this->_obj[$offset])
			: self::none();
	}

	function offsetSet($offset, $value)
	{
		return $this->offsetExists($offset)
			? self::make($this->_obj[$offset] = $value) // assign and wrap right away
			: self::none();
	}

	function offsetUnset($offset)
	{
		if (is_array($this->_obj))
			unset ($this->_obj[$offset]);
	}

	// wrap results from an actual method call
	function __call($name,  $args)
	{
		return $this->_obj !== null
			? self::make(call_user_func_array(array($this->_obj, $name), $args))
			: self::none();
	}

	// wrap a field value
	function __get($name)
	{
		return $this->_obj
			? self::make($this->_obj->$name)
			: self::none();
	}
}

function base_url( $path = "" )
{
	return base_app_uri() . $path;
}

function assets_url( $path = "" )
{
	return base_app_uri() . "/public" . $path;
}

/** daripada pakai echo, apakah gak lebih baik pake return aja? dan ketika memanggil baru pake echo p_base_url($pathnya) */
function p_base_url( $path = "" )
{
	echo base_app_uri() . $path;
}

function p_assets_url( $path = "" )
{
	echo base_app_uri() . "/public" . $path;
}

function base_app_uri()
{
	$root = (isset($_SERVER[ 'HTTPS' ]) ? "https://" : "http://") . $_SERVER[ 'HTTP_HOST' ];
	$root .= str_replace(basename($_SERVER[ 'SCRIPT_NAME' ]), '', $_SERVER[ 'SCRIPT_NAME' ]);

	return str_replace("/public/", "", $root);
}

function generateRandomString( $length = 10 )
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[ rand(0, strlen($characters) - 1) ];
	}

	return $randomString;
}

function generateUuidString()
{
	//sleep(1);
	return generateRandomString(8) . uniqid(generateRandomString(4)) . uniqid(uniqid() . "") . generateRandomString(5);
}

function generateUuid()
{
	list($usec, $sec) = explode(" ", microtime());

	return str_replace(".", "", ((float)$usec + (float)$sec) . "");
}

function startsWith( $string, $searchString)
{
	// search backwards starting from haystack length characters from the end
	return $searchString === "" || strrpos($string, $searchString, -strlen($string)) !== FALSE;
}

function endsWith( $string, $searchString )
{
	// search forward starting from end minus needle length characters
	return $searchString === "" || strpos($string, $searchString, strlen($string) - strlen($searchString )) !== FALSE;
}

function parsePost( $prefix )
{
	$arr = array();
	foreach ($_POST as $key => $value) {
		if ( startsWith($key, $prefix) ) {
			$arr[ substr($key, strlen($prefix)) ] = $value;
		}
	}

	return $arr;
}

function parseArrayByPrefix( $prefix, $arrayData )
{
	$arr = array();
	foreach ($arrayData as $key => $value) {
		if ( startsWith($key, $prefix) ) {
			$arr[ substr($key, strlen($prefix)) ] = $value;
		}
	}

	return $arr;
}

function object_to_array($data,$isUtf8=FALSE)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }else if (is_string ($data) && $isUtf8) {
        return utf8_encode($data);
    }else{
        return $data;
    }
}

function json_encode_utf8($data){
    $res = object_to_array($data,TRUE);
    return json_encode($res);
}

function is_found($data){
    if(is_array($data)){
        //echo "A".gettype($data);
        return count($data)>0;
    }else if(is_object($data)){
        $type = get_class($data);
        if($type=="Phalcon\\Mvc\\Model\\Resultset\\Simple"){
            //echo "B1";
            return count($data)>0;
        }else{
            //echo "B2";
            return $data;
        }
    }else{
        //echo "C".get_class($data);
        return $data;
    }
}

function getAppDirectory()
{
    return preg_replace('/\W\w+\s*(\W*)$/', '$1', $GLOBALS[ 'PUBLIC_DIR' ]);
}