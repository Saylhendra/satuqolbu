<?php
$GLOBALS[ 'DATABASE_APP' ] = array(
	'adapter' => 'Mysql',
	'host' => 'localhost',
	//'127.0.0.1',
	'username' => 'root',
	'password' => 'root',
	'dbname' => 'satuqolb_myhlq',
);
$GLOBALS[ 'APPLICATION' ] = array(
	'controllersDir' => __DIR__ . '/../../app/controllers/',
	'modelsDir' => __DIR__ . '/../../app/models/',
	'viewsDir' => __DIR__ . '/../../app/views/',
	'pluginsDir' => __DIR__ . '/../../app/plugins/',
	'libraryDir' => __DIR__ . '/../../app/library/',
	'languagesDir' => __DIR__ . '/../../app/languages/',
	'servicesDir' => __DIR__ . '/../../app/services/',
	'objectsDir' => __DIR__ . '/../../app/objects/',
	'cacheDir' => __DIR__ . '/../../app/cache/',
	'baseUri' => '/kdg/',
	'debugSql' => FALSE,
	'encryptJs' => FALSE
);

return new \Phalcon\Config(array(
	'database' => $GLOBALS[ 'DATABASE_APP' ],
	'application' => $GLOBALS[ 'APPLICATION' ]
));
