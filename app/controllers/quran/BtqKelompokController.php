<?php
namespace QuranModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class BtqKelompokController extends \ControllerBase
{

    private $MODEL = "SqBtqGroup";
    private $TITLE = "Data Kelompok";
    private $URL = "btq_kelompok";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('quran/'.$this->URL.'/btq_kelompok_index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(nama like :search:) ORDER BY created_date DESC";
        $bind = array("search"=>"%".$search."%");

//        $conditions = "is_membina=:is_membina: and ".$conditions;
//        $bind["is_membina"] = \StatusMembina::$MEMBINA;

        $listData = \SqBtqGroup::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));


        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_tim.png');
            $list[$idx]["created_date"] = date('d-M-y',strtotime(\Safe::make($data)->created_date->safe_get('')));
            $list[$idx]["nama"] = \Safe::make($data)->nama->safe_get('');
            $list[$idx]["pengajar"] = \Safe::make($data)->SqBtqMentor->nama->safe_get('');
//            $list[$idx]["alamat"] = \Safe::make($data)->alamat->safe_get('');
//            $list[$idx]["ttl"] = \Safe::make($data)->tempat_lahir->safe_get('').", ".date('d-M-y',strtotime(\Safe::make($data)->tgl_lahir->safe_get('')));
//            $list[$idx]["created_date"] = date('d-M H:i',strtotime(\Safe::make($data)->created_date->safe_get('')));
//            $list[$idx]["mobile"] = \Safe::make($data)->mobile->safe_get('');
//            $list[$idx]["email"] = \Safe::make($data)->email->safe_get('');
//            $list[$idx]["asal_sekolah"] = \Safe::make($data)->sqmstsekolah->nm_sekolah->safe_get('');
//            $list[$idx]["is_membina"] = \Safe::make($data)->is_membina->safe_get('');
//            $list[$idx]["photo_complaint"]=$img;
//            $list[$idx]["description"]=substr($list[$idx]["description"],0,50).'..';
//            $list[$idx]["time_reported"]=date('d/m/Y H:i:s',$data->time_reported);
            $idx++;
        }
        $count = \SqBtqGroup::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = \SqBtqGroup::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \SqBtqGroup::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array(
                "aidi" => $id
            )
        ));
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('quran/'.$this->URL.'/btq_kelompok_form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('quran/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $id_admin = isset($_GET['id_mr'])?$_GET['id_mr']:"0";

        $dataObj = \PgAdmin::findFirstById($id_admin);
        $dataObj2 = \SqCustomer::findFirstById($dataObj->id_customer);

        $model = array();
        $model['id'] = $id;
        $model['id_mr'] = $dataObj2->id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('quran/'.$this->URL.'/btq_kelompok_form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \SqBtqGroup();
            $data->assign($_POST);

            $data->id = $this->uuidString();

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

//            $oldDate = $_POST['tgl_bentuk']; //15-09-2015
//            $arr = explode('/', $oldDate);
//            $newDate = $arr[2].'-'.$arr[1].'-'.$arr[0];
//            $data->tgl_bentuk = \DateTimeLibrary::parseDateIOtoSQL($oldDate);

            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');

            $data->save();
            $response->message = \T::message("all.label.message.success_save");

 //           $savingAnggota = TimController::doSaveAnggota($data);

            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }


    public function doSaveAnggota($dataTim){
        try{
            $response2 = new \ResponseObject();

            $dataAnggota = \SqCustomer::find(array(
                "conditions" => "id_mr=:idMr:",
                "bind" => array(
                    "idMr" => $dataTim->id_pimpinan
                )
            ));

            foreach ($dataAnggota as $dataA) {
                $data = new \SqTimAnggota();
                $data->id = $this->uuidString();
                $data->id_tim = $dataTim->id;
                $data->id_anggota = $dataA->id;
                $data->created_date = date('Y-m-d H:i:s');
                $data->update_date = date('Y-m-d H:i:s');
                $data->save();
            }
        }catch (\Exception $ex){
            $response2->message = $ex->getMessage();
        }
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqBtqGroup::findFirstById($id);
            $data->assign($_POST);

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            if(array_key_exists('detailfile2', $_FILES)){
                $file_image = $_FILES['detailfile2'];
                $uploadimg = \UploadLibrary::upload_document($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_file = $uploadimg['path'];
                }
            }

            //$data->status = 0; //0 = INSERTED, 1 = DEFAULT
            $data->update_date = date('Y-m-d H:i:s');
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");

            $dataAnggota = \SqTimAnggota::find(array(
                "id_tim=:idTim:",
                "bind" => array(
                    "idTim" => $id
                )
            ));

            foreach ($dataAnggota as $dataA) {
                $dataA->delete();
            }

            $data = \SqBtqGroup::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function getBinaanAction(){
        $id = $this->request->getQuery('id_mr');
        echo json_encode(\SqCustomer::find(array(
            "conditions" => "id_mr=:idMr:",
            "bind" => array(
                "idMr" => $id
            )
        ))->toArray());
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }

}

