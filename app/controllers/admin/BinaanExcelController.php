<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class BinaanExcelController extends AdminControllerBase
{

    private $MODEL = "SqCustomer";
    private $TITLE = "Data Binaan";
    private $URL = "binaan";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(email like :search: or
                first_name like :search: or
                    last_name like :search: or
                        alamat like :search:) ORDER BY update_date DESC";
        $bind = array("search"=>"%".$search."%");

        if( !empty($this->session->is_admin) ){
            $conditions = "id_mr =:idMr: and ".$conditions;
            $bind["idMr"] = $this->session->objCustomer->id;
        }

        $conditions = "tipe in (:sts1:) and ".$conditions;
        $bind["sts1"] = \TypeCustomer::$BINAAN;

        $listData = \SqCustomer::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_member.png');
            $list[$idx]["first_name"] = \Safe::make($data)->first_name->safe_get('')." ".\Safe::make($data)->last_name->safe_get('');
            $list[$idx]["alamat"] = \Safe::make($data)->alamat->safe_get('');
            $list[$idx]["ttl"] = \Safe::make($data)->tempat_lahir->safe_get('').", ".date('d-M-y',strtotime(\Safe::make($data)->tgl_lahir->safe_get('')));
            $list[$idx]["jns_kelamin"] = \JnsKelamin::getStatus(\Safe::make($data)->jns_kelamin->safe_get(''));

            $list[$idx]["created_date"] = date('d-M H:i',strtotime(\Safe::make($data)->created_date->safe_get('')));
            $list[$idx]["update_date"] = date('d-M H:i',strtotime(\Safe::make($data)->update_date->safe_get('')));

            $dataMr = \SqCustomer::findFirstById(\Safe::make($data)->id_mr->safe_get(''));
            $list[$idx]["mr"] = empty($dataMr) ? "": $dataMr->first_name;

            $list[$idx]["mobile"] = \Safe::make($data)->mobile->safe_get('');
            $list[$idx]["email"] = \Safe::make($data)->email->safe_get('');
            $list[$idx]["kelas"] = \TypeCustomer::getStringStatus(\Safe::make($data)->status_penugasan->safe_get(99))."-".\Safe::make($data)->ket->safe_get("");

            $idx++;
        }
        $count = \SqCustomer::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = \SqCustomer::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \SqCustomer::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array(
                "aidi" => $id
            )
        ));
        $data = $data->toArray();
        $data['tgl_lahir'] = \DateTimeLibrary::parseDateSQLtoIO($data['tgl_lahir']);

        if($data)
            echo json_encode($data);
        else
            echo json_encode(array());
    }

    public function saveNewBinaanUserAction(){
        $response = new \ResponseObject();
        $this->db->begin();
        try{
            $data = new \SqCustomer();
            $data->assign($_POST);
            $data->id = $this->uuidString();

            if (array_key_exists('detailfile', $_FILES)) {
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->tipe = \TypeCustomer::$BINAAN;
            //$data->id_mr = $this->session->objCustomer->id;
            //$data->tgl_lahir = \DateTimeLibrary::parseDateIOtoSQL(empty($data->tgl_lahir)?date('Y-m-d H:i:s'):$data->tgl_lahir);
            /*=========================================*/
            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');
            $data->save();

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch(\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

//        $dataObj = \PgAdmin::findFirstById($this->session->id_admin);
//        $dataObj2 = \SqCustomer::findFirstById($dataObj->id_customer);
//        $model['id_mr'] = $dataObj2->id;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('admin/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        try {

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_document($file_image);
                if ($uploadimg['isSuccess']) {
                    $extension = $uploadimg['extension'];
                    $name = $uploadimg['name'];
                    $path = $uploadimg['path'];

                    $dataFile = $_SERVER['DOCUMENT_ROOT'].$path;
                    //$dataFile = $_SERVER['DOCUMENT_ROOT']."/mhalaqoh".$path;
                    //$dirName = str_replace("\\", "/", dirname($path)) . "/../..";

                    try {
                        $inputFileType = \PHPExcel_IOFactory::identify($dataFile);
                        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($dataFile);
                        $rows = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                        $xls_fields = isset($rows[1]) ? $rows[1] : array();
                        if(!empty($xls_fields))
                            unset($rows[1]);

                        $fields = array();
                        foreach ($xls_fields as $field) {
                            $fields[] = strtolower($field);
                        }

                    } catch(\Exception $e) {
                        die('Error loading file "'.pathinfo($dataFile,PATHINFO_BASENAME).'": '.$e->getMessage());
                    }

                    //  Get worksheet dimensions
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();

                    $daftarSiswa = array();
                    for ($row = 2; $row < $highestRow; $row++){
                        $titles = $sheet->rangeToArray('A1:' . $highestColumn . "1");
                        //  Read a row of data into an array
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL, TRUE, FALSE);

                        $hasil = array();
                        for($i = 0; $i < 4; $i++){
                            //$titles[0][1]." ".$rowData[0][2];
                            $pecahan = array(
                                $titles[0][$i] => $rowData[0][$i]
                            );
                            $hasil[] = $pecahan;
                        }
                        $daftarSiswa[] = $hasil;
                    }

                    foreach ($daftarSiswa as $x) {
                        $response = new \ResponseObject();
                        $this->db->begin();

                        $data = new \SqCustomer();
                        $data->assign($_POST);
                        $data->id = $this->uuidString();

                        /*\LoggerLibrary::logDebug($x[1]['NIS']);
                        \LoggerLibrary::logDebug($x[2]['NAMA']);*/
                        $data->first_name = $x[2]['NAMA'];
                        $data->jns_kelamin = $x[3]['KELAMIN'] == "L" ? \JnsKelamin::$IKHWAN : \JnsKelamin::$AKHWAT;
                        $data->is_membina = \StatusMembina::$BELUM;
                        $data->tipe = $data->status_penugasan;
                        //$data->status_penugasan = \TypeCustomer::$MEMBER_SD01;

                        $data->created_date = date('Y-m-d H:i:s');
                        $data->update_date = date('Y-m-d H:i:s');

                        $data->save();
                        $response->message = \T::message("all.label.message.success_save");
                        $this->db->commit();
                    }
                }
            }
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqCustomer::findFirstById($id);
            $data->assign($_POST);

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->tgl_lahir = \DateTimeLibrary::parseDateIOtoSQL($data->tgl_lahir);

            $data->update_date = date('Y-m-d H:i:s');
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqCustomer::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function get_sub_districtAction(){
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_regency = $this->request->getQuery('id_regency');
        $id_regencies = $this->request->getQuery('id_regencies');

        $conditions = "1=1 ";
        $bind = array();
        if ( isset($id) ) {
            $conditions .= " and id = :id:";
            $bind[ 'id' ] = $id;
        }
        if ( isset($ids) ) {
            $conditions .= " and id in (" . $ids . ")";
            $bind[ 'ids' ] = $ids;
        }
        if ( isset($id_regency) ) {
            $conditions .= " and id_regency = :id_regency:";
            $bind[ 'id_regency' ] = $id_regency;
        }
        if ( isset($id_regencies) ) {
            $conditions .= " and id_regency in (" . $id_regencies . ")";
            $bind[ 'id_regencies' ] = $id_regencies;
        }
        echo json_encode(\PgMstSubDistrict::find(array(
            "conditions" => $conditions,
            "bind" => $bind
        ))->toArray());
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }

}

