<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MasterAdminController extends AdminControllerBase
{

    private $MODEL = "PgAdmin";
    private $TITLE = "Data Admin";
    private $URL = "master_admin";


    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }
	
	public function listAction()
	{
		$start = isset($_GET['start'])?$_GET['start']:0;
		$length = isset($_GET['length'])?$_GET['length']:10;
		$draw = isset($_GET['draw'])?$_GET['draw']:0;
		$search = isset($_GET['search'])?$_GET['search']['value']:"";
		$conditions = "username like :search: or
		nama like :search: or
		nip like :search: or
		email like :search: or
		keterangan like :search: ORDER BY is_admin DESC";

        $list = array();
        $listData = \PgAdmin::find(array(
            "conditions" => $conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind" => array("search" => "%" . $search . "%")
        ));
        foreach($listData as $data){
            $temp = $data->toArray();
            $temp['nama_bagian'] = $data->division?$data->division->name:"";
            $temp["path_small"] = MasterAdminController::isMember($data->email, 1);//\Safe::make($d)->path_small->safe_get('/img/unknown_member.png');
            $temp["id_customer"] = MasterAdminController::isMember($data->email, 0);
            $temp["jabatan1"] = $data->SqCustomer ? \TipeJabatan::getStringStatus($data->SqCustomer->jabatan1): "";
            $temp["divisi"] = $data->Division ? $data->Division->name: "";
            $list[] = $temp;
        }
		$count = \PgAdmin::count(array(
					"conditions"=>$conditions,
					"bind"=>array("search"=>"%".$search."%")
				));
		$total = $count;

		$results = array(
            "draw" => $draw,
			"recordsTotal" => $total,
			"recordsFiltered" => $count,
			"data"=>$list
		);
		echo json_encode($results);
	}

    public function isMember($idCustomer, $type){
        $yes = null;
        $data = \SqCustomer::findFirstByEmail($idCustomer);

        if($type==0){
            if($data){
                $yes = 1;
            }else{
                $yes = 0;
            }
        }else{
            if($data){
                $yes = $data->path_small;
            }else{
                $yes = "/img/unknown_member.png";
            }
        }
        return $yes;
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $is_edit = isset($_GET['id'])?$_GET['id']:"0";

        $model = array();
        $model['id'] = $id;
        $model['is_edit'] = $is_edit;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function generateAccAction(){
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;

        $listCustomer = \SqCustomer::find();
        foreach ($listCustomer as $customer) {
            $account = \PgAdmin::findFirstByEmail($customer->email);
            if($account){
                $account->username = $customer->email;

                $arr = explode('@', $customer->email);
                $account->password = md5($arr[0]);
                $account->update();
            }
        }
        $this->view->partial('admin/'.$this->URL.'/index',$model);
    }

	public function editAction()
	{
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $is_edit = isset($_GET['isedit'])?$_GET['isedit']:"false";

        $model = array();
        $model['id'] = $id;
        $model['is_edit'] = $is_edit;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
	}

    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \PgAdmin::findFirstById($id);
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \PgAdmin();
            $data->assign($_POST);
            $data->password = md5($data->password);
            $data->id = $this->uuidString();

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->time_created = date("Y-m-d H:i:s");
            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');
            $data->save();

            $saveCustomer = MasterAdminController::doCreateCustomer($data);

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function doCreateCustomer($dataAdmin){
        $response = new \ResponseObject();
        try {
            $customer = \SqCustomer::findFirst(array(
                "id=:aidi: or email=:email:",
                "bind" => array(
                    "aidi"=>$dataAdmin->id_customer,
                    "email"=>$dataAdmin->email
                )
            ));

            if(!$customer){
                $data = new \SqCustomer();

                $data->id = $this->uuidString();

                $data->first_name = $dataAdmin->nama;
                $data->email = $dataAdmin->email;

                $data->path_small = $dataAdmin->path_small;
                $data->path_medium = $dataAdmin->path_medium;
                $data->path_large = $dataAdmin->path_large;
                $data->path_thumbnails = $dataAdmin->path_thumbnails;

                $data->created_date = date('Y-m-d H:i:s');
                $data->update_date = date('Y-m-d H:i:s');
                $data->save();
            }
        }catch (\Exception $ex){
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        return $response;
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgAdmin::findFirstById($id);
            $pass = $data->password;
            $data->assign($_POST);

            /*if($pass == md5($_POST['password'])){
                $data->password = $pass;
            }
            else{
                $data->password = md5($_POST['password']);
            }*/
            $data->password = $pass;

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->update_date = date('Y-m-d H:i:s');
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgAdmin::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
}