<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KmrController extends AdminControllerBase
{

    private $MODEL = "SqCustomer";
    private $TITLE = "Data Kantung Murrobi";
    private $URL = "kmr";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(email like :search: or
                first_name like :search: or
                    last_name like :search: or
                        alamat like :search:)
                            ORDER BY update_date DESC";
        $bind = array("search"=>"%".$search."%");

        $conditions = "status_penugasan in (:sts4:) OR is_membina=1 and ".$conditions;
        $bind["sts4"] = \TypeCustomer::$MR;

        $listData = \SqCustomer::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_member.png');
            $list[$idx]["first_name"] = \Safe::make($data)->first_name->safe_get('')." ".\Safe::make($data)->last_name->safe_get('');
            $list[$idx]["alamat"] = \Safe::make($data)->alamat->safe_get('');
            $list[$idx]["ttl"] = \Safe::make($data)->tempat_lahir->safe_get('').", ".date('d-M-y',strtotime(\Safe::make($data)->tgl_lahir->safe_get('')));

            $list[$idx]["created_date"] = date('d-M H:i',strtotime(\Safe::make($data)->created_date->safe_get('')));
            $list[$idx]["update_date"] = date('d-M H:i',strtotime(\Safe::make($data)->update_date->safe_get('')));

            $list[$idx]["mobile"] = \Safe::make($data)->mobile->safe_get('');
            $list[$idx]["email"] = \Safe::make($data)->email->safe_get('');
            $list[$idx]["asal_sekolah"] = \Safe::make($data)->sqmstsekolah->nm_sekolah->safe_get('');
            $list[$idx]["is_membina"] = \StatusMembina::getStatus(\Safe::make($data)->is_membina->safe_get(99));
            $list[$idx]["status"] = \TypeCustomer::getStringStatus(\Safe::make($data)->status_penugasan->safe_get(99));
//            $list[$idx]["path_file"] = \Safe::make($data)->path_file->safe_get('');
//            $list[$idx]["photo_complaint"]=$img;
//            $list[$idx]["description"]=substr($list[$idx]["description"],0,50).'..';
//            $list[$idx]["time_reported"]=date('d/m/Y H:i:s',$data->time_reported);
            $idx++;
        }
        $count = \SqCustomer::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = $count;

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \SqCustomer::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array(
                "aidi" => $id
            )
        ));
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $is_edit = isset($_GET['id'])?$_GET['id']:"0";

        $model = array();
        $model['id'] = $id;
        $model['is_edit'] = $is_edit;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('admin/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $is_edit = isset($_GET['isedit'])?$_GET['isedit']:"false";

        $model = array();
        $model['id'] = $id;
        $model['is_edit'] = $is_edit;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        $response = new \ResponseObject();
        try {
            $this->db->begin();
            $data = new \SqCustomer();
            $data->assign($_POST);

            $data->id = $this->uuidString();
            if(!empty($this->session->id_division) && !$this->session->isAdmin){
                $data->id_division = $this->session->id_division;
            }

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

//            if(array_key_exists('detailfile2', $_FILES)){
//                $file_image = $_FILES['detailfile2'];
//                $uploadimg = \UploadLibrary::upload_document($file_image);
//                if ($uploadimg['isSuccess']) {
//                    $data->path_file = $uploadimg['path'];
//                }
//            }
            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');
            $data->save();

            if( $_POST['create_account'] ){
                $responseCreateAccount = CustomerController::doCreateAccount($data);
            }

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function doCreateAccount($dataCustomer){
        $response = new \ResponseObject();
        try {
            $data = new \PgAdmin();

            $data->id = $this->uuidString();
            $data->id_customer = $dataCustomer->id;

            $data->username = $dataCustomer->email;
            $data->email = $dataCustomer->email;
            $data->password = md5($dataCustomer->first_name."1q");

            $data->nama = $dataCustomer->first_name." ".$dataCustomer->last_name;
            $data->keterangan = "Dibuat dari Customer";
            $data->is_admin = 0;

            $data->time_created = date("Y-m-d H:i:s");

            $data->path_small = $dataCustomer->path_small;
            $data->path_medium = $dataCustomer->path_medium;
            $data->path_large = $dataCustomer->path_large;
            $data->path_thumbnails = $dataCustomer->path_thumbnails;

            $data->created_date = date('Y-m-d H:i:s');
            $data->save();

            $response->message = \T::message("all.label.message.success_save");
        }catch (\Exception $ex){
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqCustomer::findFirstById($id);
            $data->assign($_POST);

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->update_date = date('Y-m-d H:i:s');
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqCustomer::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function getCustomerAction(){
        $this->db->begin();
        $id = $this->request->getQuery('id');
        $response = new \ResponseObject();

        try{
            $data = \SqCustomer::findFirstById($id);
            $response->data = $data;
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }

}

