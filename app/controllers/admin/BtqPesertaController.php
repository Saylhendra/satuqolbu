<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class BtqPesertaController extends AdminControllerBase
{

    private $MODEL = "SqCustomer";
    private $TITLE = "Data Peserta BTQ";
    private $URL = "peserta_btq";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/btq_peserta_index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(email like :search: or
                first_name like :search: or
                    last_name like :search: or
                        alamat like :search:) ORDER BY update_date DESC";
        $bind = array("search"=>"%".$search."%");

        if( !empty($this->session->is_admin) ){
            $conditions = "id_mr =:idMr: and ".$conditions;
            $bind["idMr"] = $this->session->objCustomer->id;
        }

        /*$conditions = "tipe =:tipeTim: and ".$conditions;
        $bind["tipeTim"] = \TipeTim::$HALAQOH_PKP;*/

        $conditions = "status_penugasan in (:sts1:, :sts4:) and ".$conditions;
        $bind["sts1"] = \TypeCustomer::$MEMBER_SD01;
        $bind["sts4"] = \TypeCustomer::$MEMBER_SD04;

        $listData = \SqCustomer::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_person.jpg');
            $list[$idx]["first_name"] = \Safe::make($data)->first_name->safe_get('')." ".\Safe::make($data)->last_name->safe_get('');
            $list[$idx]["alamat"] = \Safe::make($data)->alamat->safe_get('');
            $list[$idx]["ttl"] = \Safe::make($data)->tempat_lahir->safe_get('').", ".date('d-M-y',strtotime(\Safe::make($data)->tgl_lahir->safe_get('')));

            $list[$idx]["created_date"] = date('d-M H:i',strtotime(\Safe::make($data)->created_date->safe_get('')));
            $list[$idx]["update_date"] = date('d-M H:i',strtotime(\Safe::make($data)->update_date->safe_get('')));

            $list[$idx]["mobile"] = \Safe::make($data)->mobile->safe_get('');
            $list[$idx]["email"] = \Safe::make($data)->email->safe_get('');
            $list[$idx]["status"] = \TypeCustomer::getStringStatus(\Safe::make($data)->status_penugasan->safe_get(99));
//            $list[$idx]["path_file"] = \Safe::make($data)->path_file->safe_get('');
//            $list[$idx]["photo_complaint"]=$img;
//            $list[$idx]["description"]=substr($list[$idx]["description"],0,50).'..';
//            $list[$idx]["time_reported"]=date('d/m/Y H:i:s',$data->time_reported);
            $idx++;
        }
        $count = \SqCustomer::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = $count;

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \SqCustomer::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array(
                "aidi" => $id
            )
        ));
        $data = $data->toArray();
        $data['tgl_lahir'] = \DateTimeLibrary::parseDateSQLtoIO($data['tgl_lahir']);//date("d/m/Y",$data['tgl_lahir']);
        if($data)
            echo json_encode($data);
        else
            echo json_encode(array());
    }

    public function newBinaanUserAction(){
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

//        $dataObj = \PgAdmin::findFirstById($this->session->id_admin);
//        $dataObj2 = \SqCustomer::findFirstById($dataObj->id_customer);
//        $model['id_mr'] = $dataObj2->id;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/btq_peserta_form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('admin/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/btq_peserta_form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \SqCustomer();
            $data->assign($_POST);

            $data->id = $this->uuidString();
            if(!empty($this->session->id_division) && !$this->session->isAdmin){
                $data->id_division = $this->session->id_division;
            }

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
//                $uploadimg = \UploadLibrary::upload_picture($file_image);
//                if ($uploadimg['isSuccess']) {
//                    $data->path_small = $uploadimg['path_small'];
//                    $data->path_medium = $uploadimg['path_medium'];
//                    $data->path_large = $uploadimg['path_large'];
//                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
//                }
                $inputFileName = $file_image;
                try {
                    //$pxl = new \PHPExcel();
//                    $pxl->addExternalSheet($inputFileName);
//                    $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
//                    $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
//                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
//                    $objPHPExcel = $objReader->load($inputFileName);
                } catch(\Exception $e) {
                    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

                //  Get worksheet dimensions
//                $sheet = $objPHPExcel->getSheet(0);
//                $highestRow = $sheet->getHighestRow();
//                $highestColumn = $sheet->getHighestColumn();
//
//                for ($row = 1; $row <= $highestRow; $row++){
//                    //  Read a row of data into an array
//                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
//                        NULL,
//                        TRUE,
//                        FALSE);
//                    //  Insert row data array into your database of choice here
//                }

            }

            $oldDate = $_POST['tgl_lahir'];
            $newDate = \DateTimeLibrary::parseDateIOtoSQL($oldDate);
            $data->tgl_lahir = $newDate;

            $data->is_membina = \StatusMembina::$BELUM;
            $data->tipe = \TipeTim::$HALAQOH_PKP;

            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');
            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqCustomer::findFirstById($id);
            $data->assign($_POST);

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $oldDate = $_POST['tgl_lahir'];
            $newDate = \DateTimeLibrary::parseDateIOtoSQL($oldDate);
            $data->tgl_lahir = $newDate;

            $data->update_date = date('Y-m-d H:i:s');
            $data->update();

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqCustomer::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function get_sub_districtAction(){
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_regency = $this->request->getQuery('id_regency');
        $id_regencies = $this->request->getQuery('id_regencies');

        $conditions = "1=1 ";
        $bind = array();
        if ( isset($id) ) {
            $conditions .= " and id = :id:";
            $bind[ 'id' ] = $id;
        }
        if ( isset($ids) ) {
            $conditions .= " and id in (" . $ids . ")";
            $bind[ 'ids' ] = $ids;
        }
        if ( isset($id_regency) ) {
            $conditions .= " and id_regency = :id_regency:";
            $bind[ 'id_regency' ] = $id_regency;
        }
        if ( isset($id_regencies) ) {
            $conditions .= " and id_regency in (" . $id_regencies . ")";
            $bind[ 'id_regencies' ] = $id_regencies;
        }
        echo json_encode(\PgMstSubDistrict::find(array(
            "conditions" => $conditions,
            "bind" => $bind
        ))->toArray());
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }

}

