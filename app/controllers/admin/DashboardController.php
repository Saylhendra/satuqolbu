<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class DashboardController extends AdminControllerBase
{

    private $MODEL = "PgAdmin";
    private $TITLE = "Panel";
    private $URL = "dashboard";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }
    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";
        $conditions = "description like :search: ";
        $list = array();
        $data = \PgComplaint::find(array(
            "conditions"=>$conditions." LIMIT ".$length." OFFSET ".$start,
            "bind"=>array("search"=>"%".$search."%")
        ));
        $no = 1;
        foreach($data as $d){
            $temp = $d->toArray();
            $temp['no'] = $no;
            $temp['tgl'] = date('d/m/Y H:i:s',$d->time_reported);
            $temp['category_name'] = \Safe::make($d)->category->name->safe_get('/dist/img/boxed-bg.jpg');
            $temp['gambar'] = '<img src="'.assets_url(\Safe::make($d)->PgSource->img->safe_get('')).'" width="32"/>';
            $temp['status_label'] = \ComplaintStatus::getStatus($d->status_process);
            $temp['action'] = '<div class="input-group-btn">'.
                              '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="fa fa-caret-down"></span></button>'.
                                '<ul class="dropdown-menu">'.
                                    '<li><a href="#" onclick="viewDetailComplaint(\''.$d->id.'\')">Detail</a></li>'.
                                    '<li><a href="#" onclick="openFormPengaduanEdit(\''.$d->id.'\')">Edit Data</a></li>'.
                                    '<li><a href="#" onclick="confirmRemovePengaduan(\''.$d->id.'\')">Hapus Data</a></li>'.
                                '</ul>'.
                               '</div>';
            $list[] = $temp;
            $no++;
        }
        $count = \PgComplaint::count(array(
            "conditions"=>$conditions,
            "bind"=>array("search"=>"%".$search."%")
        ));
        $total = \PgComplaint::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }
    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \PgComplaint();
            $data->assign($_POST);
            $data->source=$_POST['source'];
            $time_repoted = $_POST['time_reported'].":00";
            $data->time_reported = strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($time_repoted));
            $data->id = $this->uuidString();
            $data->time_created = time();
            if(!empty($this->session->id_division) && !$this->session->isAdmin){
                $data->id_division = $this->session->id_division;
            }
            $file_image = isset($_FILES['detailfile'])?$_FILES['detailfile']:false;
            if($file_image) {
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->photo_complaint = $uploadimg['path_medium'];
                }
            }

            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgComplaint::findFirstById($id);
            $data->assign($_POST);
            $data->source=$_POST['source'];
            $time_repoted = $_POST['time_reported'].":00";
            $data->time_reported = strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($time_repoted));
            $file_image = isset($_FILES['detailfile'])?$_FILES['detailfile']:false;
            if($file_image) {
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->photo_complaint = $uploadimg['path_medium'];
                }
            }
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
    public function getAction()
    {
        $id = $this->request->getQuery("id");
        $data = \PgComplaint::findFirstById($id);
        $data = $data->toArray();
        $data['time_reported'] = date("d/m/Y H:i",$data['time_reported']);
        echo json_encode($data);
    }
    public function tableDataAction(){
        $this->view->partial('admin/dashboard/list_data');
    }
    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgComplaint::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
    public function testAction(){
        $time = time();
        echo $time;
        echo "<br/>";
        $time2 = date("d/m/Y H:i:s",$time);
        echo $time2;
        echo "<br/>";
        //$time2 = "21/10/2015 00:20:00";
        $time2Temp = \DateTimeLibrary::parseDateTimeIOtoSQL($time2);
        echo $time2Temp;
        echo "<br/>";
        $time2Temp = strtotime($time2Temp);
        echo $time2Temp;
        echo "<br/>";
        $time3 = date("d/m/Y H:i:s",$time2Temp);
        echo $time3;
    }
    public function viewAction()
    {
        $id = $this->request->getQuery("id");
        $data = \PgComplaint::findFirstById($id);
        $model = array();
        $model['complaint'] = $data;
        $this->view->partial('admin/dashboard/view',$model);
    }
    public function listQlueAction()
    {
        $model = array();
        $this->view->partial('admin/dashboard/list_data_qlue',$model);
    }

    public function twitterAction(){
        $id = $this->request->getQuery('id');
        $twitter = \TwitterLibrary::getStatusById($id);
        $model = array();
        $model['data'] = $twitter;
        $this->view->partial('admin/dashboard/viewTwitter',$model);
    }
    public function qlueAction(){
        $model = array();
        $model['complaint'] = json_decode(json_encode($_POST), FALSE);
        $this->view->partial('admin/dashboard/viewQlue',$model);
    }
    public function smsAction(){
        $model = array();
        $model['complaint'] = json_decode(json_encode($_POST), FALSE);
        $this->view->partial('admin/dashboard/viewSms',$model);
    }


    public function listCustomerAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "first_name like :search:
                            or email like :search:";
        $listData = \SqCustomer::find(array(
            "conditions"=>$conditions." ORDER BY update_date DESC ",
            "limit"=>$length,
            "offset"=>$start,
            "bind"=> array("search"=>"%".$search."%")
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_member.png');
            $list[$idx]["first_name"] = \Safe::make($data)->first_name->safe_get('')." ".\Safe::make($data)->last_name->safe_get('');
            $list[$idx]["alamat"] = \Safe::make($data)->alamat->safe_get('');
            $list[$idx]["ttl"] = \Safe::make($data)->tempat_lahir->safe_get('').", ".date('d-M-y',strtotime(\Safe::make($data)->tgl_lahir->safe_get('')));
            $list[$idx]["created_date"] = date('d-M H:i',strtotime(\Safe::make($data)->created_date->safe_get('')));
            $list[$idx]["mobile"] = \Safe::make($data)->mobile->safe_get('');
            $list[$idx]["email"] = \Safe::make($data)->email->safe_get('');
            $list[$idx]["asal_sekolah"] = \Safe::make($data)->sqmstsekolah->nm_sekolah->safe_get('');
            $idx++;
        }
        $count = \SqCustomer::count(array(
            "conditions"=>$conditions,
            "bind"=>array("search"=>"%".$search."%")
        ));
        $total = \SqCustomer::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }
}