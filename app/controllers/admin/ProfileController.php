<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ProfileController extends AdminControllerBase
{

    private $MODEL = "PgAdmin";
    private $TITLE = "Profile User";
    private $URL = "profile";


    public function indexAction()
    {
        $id = $this->session->id_admin;
        $data = \PgAdmin::findFirstById($id);
        $dataCustomer = \SqCustomer::findFirstByEmail($data->email);

        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['profile'] = $data;
        $model['customer'] = $dataCustomer;

        $this->view->partial('admin/'.$this->URL.'/view',$model);//,$arrPersonnel);
    }

	public function editAction()
	{
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
	}

    public function getAction()
    {
        $id = $this->session->id_admin;
        $data = \PgAdmin::findFirstById($id);

        $data = $data->toArray();
        //$data['tgl_lahir'] = \DateTimeLibrary::parseDateSQLtoIO($data['tgl_lahir']);

        if($data)
            echo json_encode($data);
        else
            echo json_encode(array());
    }

    public function updateAction()
    {
        $id = $this->session->id_admin;
        $data = \PgAdmin::findFirstById($id);
        $pass = $data->password;
        $data->assign($_POST);

        /*if($pass == md5($_POST['password'])){
            $data->password = $pass;
        }
        else{
            $data->password = md5($_POST['password']);
        }*/

        if( !empty($_POST['prof_password']) ){
            $data->password = md5($_POST['prof_password']);
        }else{
            $data->password = $pass;
        }
        $data->update();

        if(array_key_exists('detailfile', $_FILES)){
            $file_image = $_FILES['detailfile'];
            $uploadimg = \UploadLibrary::upload_picture($file_image);
            if ($uploadimg['isSuccess']) {
                $_POST['path_small'] = $uploadimg['path_small'];
                $_POST['path_medium'] = $uploadimg['path_medium'];
                $_POST['path_large'] = $uploadimg['path_large'];
                $_POST['path_thumbnails'] = $uploadimg['path_thumbnails'];
            }
        }

        $responseBtqMentor = \MemberService::doUpdateCustomer($data, $_POST);
        //echo $pass.":".md5($_POST['password']);
        //exit();
        echo json_encode($this->getResponse());
    }
}