<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class JurnalController extends AdminControllerBase
{
    private $MODEL = "SqJurnal";
    private $TITLE = "Jurnal Halaqoh";
    private $URL = "jurnal";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(tgl_kegiatan like :search:)
            ORDER BY update_date DESC";
        $bind = array("search"=>"%".$search."%");

//        if( !empty($this->session->is_admin) || $this->session->id_customer){
//            $conditions = "id_pimpinan =:id_pimpinan: and ".$conditions;
//            $bind["id_pimpinan"] = $this->session->id_customer;
//        }

        $listData = \SqJurnal::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));

        $list = array();
        $idx = 0;
        foreach($listData as $data){

            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_tim.png');
            $list[$idx]["tipe_kegiatan"] = \TipeKegiatan::getString(\Safe::make($data)->tipe_kegiatan->safe_get(null));
            $list[$idx]["tgl_kegiatan"] = date('d-M-y H:i',strtotime(\Safe::make($data)->tgl_kegiatan->safe_get('')));
            $list[$idx]["catatan"] = \Safe::make($data)->catatan->safe_get('');
            $list[$idx]["sts_hadir_mr"] = \StatusHadir::getStatus(\Safe::make($data)->is_pimpinan_hadir->safe_get(null));
            $list[$idx]["jml_peserta"] = \Safe::make($data)->jml_peserta->safe_get(0);
//            $list[$idx]["mobile"] = \Safe::make($data)->mobile->safe_get('');
//            $list[$idx]["email"] = \Safe::make($data)->email->safe_get('');
//            $list[$idx]["asal_sekolah"] = \Safe::make($data)->sqmstsekolah->nm_sekolah->safe_get('');
//            $list[$idx]["path_file"] = \Safe::make($data)->path_file->safe_get('');
            $idx++;
        }
        $count = \SqJurnal::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = $count;

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );

        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \SqJurnal::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array("aidi" => $id)
        ));
        $data = $data->toArray();
        $data['tgl_kegiatan'] = date("d/m/Y H:i", strtotime($data['tgl_kegiatan']));
        if($data):
            echo json_encode($data);
        else:
            echo json_encode(array());
        endif;
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

        $dataObj2 = null;
//        if($this->session->id_admin){
//            $dataObj = \PgAdmin::findFirstById($this->session->id_admin);
//            $dataObj2 = \SqCustomer::findFirstById($dataObj->id_customer);
//            $dataObj2 = $dataObj2->id;
//        }else{
//            $dataObj = \PgAdmin::findFirstById($this->session->id_admin);
//            $dataObj2 = $dataObj->id;
//        }
        $model['id_mr'] = $dataObj2;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('admin/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \SqJurnal();
            $data->assign($_POST);

            $data->id = $this->uuidString();

            $data->nama = $_POST[''];

            $oldDate = $_POST['tgl_kegiatan']; //15-09-2015
            $data->tgl_kegiatan = \DateTimeLibrary::parseDateTimeSQLtoIO($oldDate);

            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');

            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function saveNewJurnalUserAction(){
        $response = new \ResponseObject();
        $this->db->begin();
        try{
            $affiliations = array();

            $data = new \SqJurnal();
            $data->assign($_POST);
            $data->id = $this->uuidString();

            $oldDate = $_POST['tgl_kegiatan']; //15-09-2015

            $data->tgl_kegiatan = \DateTimeLibrary::parseDateTimeSQLtoIO($oldDate);

            if( array_key_exists('anggota_chk',$_POST) ){
                $doc_own_chk = $_POST["anggota_chk"];
                $affiliations = array(
                    "data" => $doc_own_chk
                );
            }

            if( count($affiliations['data']) > 0 ){
                $data->jml_peserta = count($affiliations['data']);
            }

            /*=========================================*/
            $data->update_date = date('Y-m-d H:i:s');
            $data->created_date = date('Y-m-d H:i:s');

            $data->save();

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch(\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqJurnal::findFirstById($id);

            $data->assign($_POST);

            $oldDate = $_POST['tgl_kegiatan']; //15-09-2015
            $data->tgl_kegiatan = \DateTimeLibrary::parseDateTimeSQLtoIO($oldDate);

            $data->update_date = date('Y-m-d H:i:s');
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqJurnal::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function get_sub_districtAction(){
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_regency = $this->request->getQuery('id_regency');
        $id_regencies = $this->request->getQuery('id_regencies');

        $conditions = "1=1 ";
        $bind = array();
        if ( isset($id) ) {
            $conditions .= " and id = :id:";
            $bind[ 'id' ] = $id;
        }
        if ( isset($ids) ) {
            $conditions .= " and id in (" . $ids . ")";
            $bind[ 'ids' ] = $ids;
        }
        if ( isset($id_regency) ) {
            $conditions .= " and id_regency = :id_regency:";
            $bind[ 'id_regency' ] = $id_regency;
        }
        if ( isset($id_regencies) ) {
            $conditions .= " and id_regency in (" . $id_regencies . ")";
            $bind[ 'id_regencies' ] = $id_regencies;
        }
        echo json_encode(\PgMstSubDistrict::find(array(
            "conditions" => $conditions,
            "bind" => $bind
        ))->toArray());
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }

    public function getAnggotaAction(){
        $response = new \ResponseObject();
        $id = $this->request->getQuery('id_tim');
        $data = \SqTimAnggota::findByIdTim($id)->toArray();

        $response->data = $data;
        $response->message = \T::message("all.label.message.success_delete");

        echo json_encode($response->data);
    }

}

