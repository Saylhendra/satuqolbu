<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class TimController extends AdminControllerBase
{

    private $MODEL = "SqTim";
    private $TITLE = "Data Kelompok";
    private $URL = "tim";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(nama like :search:)
            ORDER BY update_date DESC";
        $bind = array("search"=>"%".$search."%");
        if(!empty($this->session->id_customer) && !$this->session->isAdmin){
            $conditions = "id_pimpinan=:idPimpinan: and ".$conditions;
            $bind["idPimpinan"] = $this->session->id_customer;
        }

        $listData = \SqTim::find(array(
            "conditions"=>$conditions,
            "limit"=>$length,
            "offset"=>$start,
            "bind"=>$bind
        ));


        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["path_small"] = \Safe::make($data)->path_small->safe_get('/img/unknown_tim.png');
            $list[$idx]["tgl_bentuk"] = date('d-M-y',strtotime(\Safe::make($data)->tgl_bentuk->safe_get('')));
            $list[$idx]["tgl_berakhir"] = date('d-M-y',strtotime(\Safe::make($data)->tgl_berakhir->safe_get('')));

            $dataCustomer = \SqTim::findFirstById(\Safe::make($data)->id->safe_get(''));
            $ss = $dataCustomer->getCustomer();
            $list[$idx]["pimpinan"] = \Safe::make($ss)->first_name->safe_get('');

            $dataAnggota = \SqTimAnggota::count(array(
                "conditions"=>"id_tim=:idTim:",
                "bind"=>array("idTim"=>\Safe::make($data)->id->safe_get(''))
            ));
            $list[$idx]["jml_anggota"] = $dataAnggota;

            $list[$idx]["nama_tim"] = \Safe::make($data)->nama->safe_get('');
            $idx++;
        }
        $count = \SqTim::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = $count;

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \SqTim::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array(
                "aidi" => $id
            )
        ));
        if(!empty($data->tgl_berakhir)){
            $data->tgl_berakhir = \DateTimeLibrary::parseDateSQLtoIO($data->tgl_berakhir);
        }
        if(!empty($data->tgl_bentuk)){
            $data->tgl_bentuk = \DateTimeLibrary::parseDateSQLtoIO($data->tgl_bentuk);
        }
        $data = $data->toArray();

        if($data)
            echo json_encode($data);
        else
            echo json_encode(array());
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;

        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('admin/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $id_admin = isset($_GET['id_mr'])?$_GET['id_mr']:"0";

        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
        //$this->view->partial('admin/'.$this->URL.'/tim_profile',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \SqTim();
            $data->assign($_POST);

            $data->id = $this->uuidString();

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->tgl_bentuk = \DateTimeLibrary::parseDateIOtoSQL($_POST['tgl_bentuk']);
            if(!empty($_POST['tgl_berakhir'])){
                $data->tgl_berakhir = \DateTimeLibrary::parseDateIOtoSQL($_POST['tgl_berakhir']);
            }

            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');

            $data->save();
            $response->message = \T::message("all.label.message.success_save");

            $savingAnggota = TimController::doSaveAnggota($data);

            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function saveNewTimUserAction(){
        $response = new \ResponseObject();
        $this->db->begin();
        try{
            $data = new \SqTim();
            $data->assign($_POST);
            $data->id = $this->uuidString();

            if (array_key_exists('detailfile', $_FILES)) {
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->tgl_bentuk = \DateTimeLibrary::parseDateIOtoSQL($_POST['tgl_bentuk']);
            if(!empty($_POST['tgl_berakhir'])){
                $data->tgl_berakhir = \DateTimeLibrary::parseDateIOtoSQL($_POST['tgl_berakhir']);
            }
            /*=========================================*/
            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');
            $data->save();

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch(\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }


    public function doSaveAnggota($dataTim){
        try{
            $response2 = new \ResponseObject();

            $dataAnggota = \SqCustomer::find(array(
                "conditions" => "id_mr=:idMr:",
                "bind" => array(
                    "idMr" => $dataTim->id_pimpinan
                )
            ));

            foreach ($dataAnggota as $dataA) {
                $data = new \SqTimAnggota();
                $data->id = $this->uuidString();
                $data->id_tim = $dataTim->id;
                $data->id_anggota = $dataA->id;
                $data->created_date = date('Y-m-d H:i:s');
                $data->update_date = date('Y-m-d H:i:s');
                $data->save();
            }
        }catch (\Exception $ex){
            $response2->message = $ex->getMessage();
        }
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \SqTim::findFirstById($id);
            $data->assign($_POST);

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->tgl_bentuk = \DateTimeLibrary::parseDateIOtoSQL($_POST['tgl_bentuk']);
            if(!empty($_POST['tgl_berakhir'])){
                $data->tgl_berakhir = \DateTimeLibrary::parseDateIOtoSQL($_POST['tgl_berakhir']);
            }

            $data->update_date = date('Y-m-d H:i:s');
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");

            $listData = \SqTimAnggota::find(array(
                "id_tim=:key1:",
                "bind" => array("key1" => $id)
            ));
            foreach ($listData as $dataA) {
                $dataA->delete();
            }

            $data = \SqTim::findFirstById($id);
            $data->delete();

            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function getBinaanAction(){
        $id = $this->request->getQuery('id_mr');
        echo json_encode(\SqCustomer::find(array(
            "conditions" => "id_mr=:idMr:",
            "bind" => array(
                "idMr" => $id
            )
        ))->toArray());
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }

}

