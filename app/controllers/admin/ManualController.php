<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class ManualController extends AdminControllerBase
{

    private $MODEL = "PgAdmin";
    private $TITLE = "Input Pengaduan";
    private $URL = "manual";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "(description like :search: or
                name_informer like :search: or
                    notes_proceed like :search: or
                        description like :search:)
                            ORDER BY time_created DESC
                                LIMIT ".$length." OFFSET ".$start;
        $bind = array("search"=>"%".$search."%");
        if(!empty($this->session->id_division) && !$this->session->isAdmin){
            $conditions = "id_division=:id_division: and ".$conditions;
            $bind["id_division"] = $this->session->id_division;
        }


        $listData = \PgComplaint::find(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $img='';
            if($data->photo_complaint!=''){
                $img=base_url($data->photo_complaint);
            }else{
                if($data->photo_complaint_social!=''){
                    $img=$data->photo_complaint_social;
                }
            }
            $list[$idx] = $data->toArray();
            $list[$idx]["name_category"] = \Safe::make($data)->category->name->safe_get('');
            $list[$idx]["source"] = '<img src="'.base_url('/public/'.$data->PgSource->img).'" width="32">';
            $list[$idx]["label_status"] = \ComplaintStatusPriority::getStatusPriority(\Safe::make($data)->status_priority->safe_get(0));
            $list[$idx]["label_status_process"] = \ComplaintStatus::getStatus(\Safe::make($data)->status_process->safe_get(0));
            $list[$idx]["label_status_respons_unit"] = \ComplaintResponsUnit::getStatus(\Safe::make($data)->status_respons_unit->safe_get(0));
            $list[$idx]["photo_complaint"]=$img;
            $list[$idx]["description"]=substr($list[$idx]["description"],0,50).'..';
            $list[$idx]["time_reported"]=date('d/m/Y H:i:s',$data->time_reported);
            $idx++;
        }
        $count = \PgComplaint::count(array(
            "conditions"=>$conditions,
            "bind"=>$bind
        ));
        $total = \PgComplaint::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \PgComplaint::findFirst(array(
            "conditions" => "id=:aidi:",
            "bind" => array(
                "aidi" => $id
            )
        ));
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function processAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = "";
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Proses";
        $this->view->partial('admin/'.$this->URL.'/form_process',$model);
    }

    public function editAction()
    {

        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \PgComplaint();
            $data->assign($_POST);
            $data->source=$_POST['source'];
            $data->id = $this->uuidString();
            $data->time_created = time();
            if(!empty($this->session->id_division) && !$this->session->isAdmin){
                $data->id_division = $this->session->id_division;
            }

            $data->photo_complaint='';
            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->photo_complaint = $uploadimg['path_medium'];
                }
            }
            $time_repoted = $_POST['time_reported'].":00";
            $data->time_reported = strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($time_repoted));

            $data->status_priority = \ComplaintStatusPriority::$HIGH;

            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgComplaint::findFirstById($id);
            $data->assign($_POST);

            $data->time_created = time();

            if(array_key_exists('detailfile', $_FILES)){
                $file_image = $_FILES['detailfile'];
                $uploadimg = \UploadLibrary::upload_picture($file_image);
                if ($uploadimg['isSuccess']) {
                    $data->photo_complaint = $uploadimg['path_medium'];
                }
            }

             $time_repoted = $_POST['time_reported'].":00";
            $data->time_reported = strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($time_repoted));

            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgComplaint::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function get_sub_districtAction(){
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_regency = $this->request->getQuery('id_regency');
        $id_regencies = $this->request->getQuery('id_regencies');

        $conditions = "1=1 ";
        $bind = array();
        if ( isset($id) ) {
            $conditions .= " and id = :id:";
            $bind[ 'id' ] = $id;
        }
        if ( isset($ids) ) {
            $conditions .= " and id in (" . $ids . ")";
            $bind[ 'ids' ] = $ids;
        }
        if ( isset($id_regency) ) {
            $conditions .= " and id_regency = :id_regency:";
            $bind[ 'id_regency' ] = $id_regency;
        }
        if ( isset($id_regencies) ) {
            $conditions .= " and id_regency in (" . $id_regencies . ")";
            $bind[ 'id_regencies' ] = $id_regencies;
        }
        echo json_encode(\PgMstSubDistrict::find(array(
            "conditions" => $conditions,
            "bind" => $bind
        ))->toArray());
    }

    public function isLocationExistAction(){
        $response = false;
        $txtLocation = $this->request->getQuery('txt_location');

        $isExist = \PgComplaint::find(array(
            "conditions" => "location like '%".":txtLocation"."%'",
            "bind" => array(
                "txtLocation" => $txtLocation
            )
        ));
        if( $isExist ){
            $response = true;
        }
        return json_decode($response);
    }
}

