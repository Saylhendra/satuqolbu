<?php
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;

class TestController extends Controller
{
	public function indexAction()
	{
        $idBusiness = $_GET['id'];
        $listBusiness = BusinessService::getTreeChildsBusiness($idBusiness);
        //echo json_encode($listBusiness);
        foreach($listBusiness as $bs)
        {
            //$bs = KdgMstBusiness::findFirstById($bsId);
            //var_dump($bs);
            if($bs->type==TypeBusiness::$SUBSIDIARY){
                echo "SUBSIDIARY :".$bs->subtitle."<br/>";
            }else if($bs->type==TypeBusiness::$DIVISION){
                echo "DIVISION :".$bs->divtitle."<br/>";
            }else{
                echo "MAIN:".$bs->business_name."<br/>";
            }
            //echo "name:".$bs['business_name']."/".$bs['subtitle']."/".$bs['divtitle']."/".$bs['type']."<br/>";
        }
	}
    public function newsApproverAction()
    {
        $idBusiness = $_GET['id'];
        $bs = KdgMstBusiness::findFirstById($idBusiness);

        $newsApprover = BusinessService::getNewsApprover($bs);
        echo "count:".count($newsApprover)."<br/>";
        //var_dump($newsApprover);
        $bs = $newsApprover->business;
        if($bs)
        {
            //$bs = KdgMstBusiness::findFirstById($bsId);
            //var_dump($bs);
            if($bs->type==TypeBusiness::$SUBSIDIARY){
                echo "SUBSIDIARY :".$bs->subtitle."<br/>";
            }else if($bs->type==TypeBusiness::$DIVISION){
                echo "DIVISION :".$bs->divtitle."<br/>";
            }else{
                echo "MAIN:".$bs->business_name."<br/>";
            }
            //echo "name:".$bs['business_name']."/".$bs['subtitle']."/".$bs['divtitle']."/".$bs['type']."<br/>";
        }
        foreach($newsApprover->approvers as  $reviewer)
        {
            $member = KdgMstMember::findFirstById($reviewer->id_member);
            echo "name reviewr:".$member->first_name." ".$member->last_name."<br/>";
        }
    }
    public function allParentAction()
    {
        $idBusiness = $_GET['id'];
        $listBusiness = BusinessService::getTreeParentsBusiness($idBusiness);
        //echo json_encode($listBusiness);
        foreach($listBusiness as $bs)
        {
            //$bs = KdgMstBusiness::findFirstById($bsId);
            //var_dump($bs);
            if($bs->type==TypeBusiness::$SUBSIDIARY){
                echo "SUBSIDIARY :".$bs->subtitle."<br/>";
            }else if($bs->type==TypeBusiness::$DIVISION){
                echo "DIVISION :".$bs->divtitle."<br/>";
            }else{
                echo "MAIN:".$bs->business_name."<br/>";
            }
            //echo "name:".$bs['business_name']."/".$bs['subtitle']."/".$bs['divtitle']."/".$bs['type']."<br/>";
        }
    }
    public function parentAction()
    {
        $idBusiness = $_GET['id'];
        $bs = BusinessService::getParentOfBusiness($idBusiness);
        //echo json_encode($listBusiness);
        if($bs)
        {
            //$bs = KdgMstBusiness::findFirstById($bsId);
            //var_dump($bs);
            if($bs->type==TypeBusiness::$SUBSIDIARY){
                echo "SUBSIDIARY :".$bs->subtitle."<br/>";
            }else if($bs->type==TypeBusiness::$DIVISION){
                echo "DIVISION :".$bs->divtitle."<br/>";
            }else{
                echo "MAIN:".$bs->business_name."<br/>";
            }
            //echo "name:".$bs['business_name']."/".$bs['subtitle']."/".$bs['divtitle']."/".$bs['type']."<br/>";
        }
    }
    public function memberNewsAction()
    {
        $idMember = $_GET['id'];
        $allNews = BusinessService::getAllNewsByIdMember($idMember);
        echo "count:".count($allNews);
        foreach($allNews as $news)
        {

            echo "id:".$news->id."<br/>";
            echo "pin title:".$news->pin_title."<br/>";
            echo "pin_description:".$news->pin_description."<br/>";
            foreach($news->getAllImage() as $image)
            {
                echo "<img src='".base_url().$image->path_small."'/>";
            }
            echo "pin title:".$news->pin_title."<br/>";
            echo "detail_title:".$news->detail_title."<br/>";
            echo "detail_description:".$news->detail_description."<br/>";
            echo "purpose:".$news->purpose."<br/>";
            echo "<br/><br/>";
        }
    }
    public function businessNewsAction()
    {
        $idBusiness = $_GET['id'];
        $allNews = BusinessService::getAllNewsByIdBusiness($idBusiness);
        echo "count:".count($allNews);
        foreach($allNews as $news)
        {

            echo "id:".$news->id."<br/>";
            echo "pin title:".$news->pin_title."<br/>";
            echo "pin_description:".$news->pin_description."<br/>";
            foreach($news->getAllImage() as $image)
            {
                echo "<img src='".base_url().$image->path_small."'/>";
            }
            echo "pin title:".$news->pin_title."<br/>";
            echo "detail_title:".$news->detail_title."<br/>";
            echo "detail_description:".$news->detail_description."<br/>";
            echo "purpose:".$news->purpose."<br/>";
            echo "<br/><br/>";
        }
    }
    public function sqlAction()
    {
        $sql = "select * from pt_admin";
        //new Phalcon\Mvc\Model()::
        $di = Phalcon\DI::getDefault();
        $result= $di['db']->query($sql);
        $result->setFetchMode(Phalcon\Db::FETCH_OBJ);
        while($row=$result->fetch())
        {
            //echo $row['id']." ".$row['username']."<br>";
            echo $row->id." ".$row->username."<br>";
        }
    }
    public function twitterAction(){
//        var_dump(TwitterLibrary::getUserTimeline(array('count'=>1,'since_id'=>2)));
//        var_dump(TwitterLibrary::getUserTimeline());
        LoggerLibrary::logDebug("test");
        echo json_encode(TwitterLibrary::getHomeTimeline(array('count'=>10)));
    }

    public function getConnectionAction(){
        var_dump(TwitterLibrary::getConnection());
    }

    public function getVerifyAction(){
        var_dump(TwitterLibrary::verify());
    }
}