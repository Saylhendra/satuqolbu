<?php

class IndexController extends \ControllerBase{

    private $URL = "page_pixlsignin";
    private $URL1 = "page_pixlhome";
    private $URL2 = "page_material";
    private $URL3 = "page_hansen";

    public function indexAction(){

        $this->response->redirect(base_url('/admin/dashboard'));
        //$this->view->partial('front/'.$this->URL.'/index');
        //$this->view->partial('front/'.$this->URL1.'/index');
        //$this->view->partial('front/'.$this->URL2.'/index');
        //$this->view->partial('front/'.$this->URL3.'/index');
    }

    public function quranAction(){
        $this->view->partial('front/'.$this->URL2.'/index');
    }

	public function initialize()
	{
		$this->init();
	}

	public function detailAction()
	{
		$response = new ResponseObject();
		$response->isSuccess = TRUE;
		echo json_encode($response);
		exit();
	}

	public function resetLangAction()
	{
		I18nLibrary::resetLang();
		$this->response->redirect(base_url('/login'));
	}

	public function setLangAction()
	{
		I18nLibrary::setLang($this->request->getQuery('lang'));
		$this->response->redirect(base_url('/login'));
	}

    public function mobileAction()
    {
        MobileService::sendMobileRegistration("aaaaaaaaaaaaaaaakhmad.mibssssssssssssssssssssssssssssssssss@bitsolution.technology","087871875353","123999");
        MobileService::sendMobileRegistration("aaaaaaaaaaaaaaaakhmad.mibssssssssssssssssssssssssssssssssss@bitsolution.technology","085283731125","123999");
    }

    public function imageAction()
    {
        $resize = new ResizeImageLibrary("D:/data/archibus1.png");
        $resize->resizeTo(32,32);
        $resize->saveImage("D:/data/archibus1_32x32.png");
    }
    public function testMongoAction()
    {
        $notifs = KdgNotificationNew::find(
            array(array("coba"=>array('$regex'=>"1.*")))
        );
        echo "There are ", count($notifs), "\n";
        echo json_encode($notifs);
        $notifs = KdgNotificationNew::find(
            array(
                array('$or'=>
                    array(
                        array("coba"=>array('$regex'=>"1.*")),
                        array("status_active"=>0)
                    )
                )
            )
        );
        echo "There are ", count($notifs), "\n";
        echo json_encode($notifs);
    }
    public function testStrAction()
    {
        $str = null;
        var_dump(empty($str));
    }
}

