<?php
namespace FrontModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class HansenController extends \ControllerBase
{

    private $URL = "page_hansen";

    public function indexAction()
    {$this->view->partial('front/'.$this->URL.'/index');}

    public function aboutAction()
    {$this->view->partial('front/'.$this->URL.'/about');}

    public function contactAction()
    {$this->view->partial('front/'.$this->URL.'/contact');}

    public function foroforAction()
    {$this->view->partial('front/'.$this->URL.'/404');}

    public function indexsliderAction()
    {$this->view->partial('front/'.$this->URL.'/index_slider');}

    public function indexvideoAction()
    {$this->view->partial('front/'.$this->URL.'/index_video');}

    public function shortcodesAction()
    {$this->view->partial('front/'.$this->URL.'/shortcodes');}

    public function comingsoonAction()
    {$this->view->partial('front/'.$this->URL.'/comingsoon');}

}

