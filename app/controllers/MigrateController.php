<?php
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MigrateController extends ControllerBase
{

	private $importFile = "all_table_schema_15_maret_2015_bowo.sql";

	public function importAction()
	{
		$db = $GLOBALS[ 'DATABASE_APP' ];
		$dirName = str_replace("\\", "/", dirname(__FILE__)) . "/../..";
		$fileImport = $dirName . "/db/" . $this->importFile;
		DatabaseLibrary::importSql($db[ "host" ], $db[ "username" ], $db[ "password" ], $db[ "dbname" ], $fileImport, TRUE);
	}

	public function exportAction()
	{
		$regency = KdgMstRegency::findFirst();
		echo "regency:" . $regency->nama;
		echo "<br/>";
		//print_r($regency);
		echo "province:" . $regency->kdgmstprovince->nama;
		echo "<br/>";
		echo "country:" . $regency->kdgmstprovince->kdgmstcountry->country_name;
		$lob4 = KdgMstLob::findFirst(array(
			"conditions" => "code_level=4"
		));
		echo "<br/>";
		echo "lob4:" . $lob4->code;
		echo "<br/>";
		echo "lob3:" . $lob4->kdgmstlob->code;
		echo "<br/>";
		echo "lob2:" . $lob4->kdgmstlob->kdgmstlob->code;
		echo "<br/>";
		echo "lob1:" . $lob4->kdgmstlob->kdgmstlob->kdgmstlob->code;
		echo "<br/>";
		echo "count:" . count($lob4->kdgmstlob->kdgmstlob->kdgmstlob->childs);
		echo "<br/>";
		foreach ($lob4->kdgmstlob->kdgmstlob->kdgmstlob->childs as $c) {
			echo "lob child:" . $c->code;
			echo "<br/>";
		}
	}


}

