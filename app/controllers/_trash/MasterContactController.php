<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MasterContactController extends AdminControllerBase
{

    private $MODEL = "PgContact";
    private $TITLE = "Kontak";
    private $URL = "master_contact";


    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";
        $conditions = "name like :search: or list_group like :search: or email like :search: or phone=:search: LIMIT ".$length." OFFSET ".$start;
        $data = \ViewPgContact::find(array(
            "conditions"=>$conditions,
            "bind"=>array("search"=>"%".$search."%")
        ))->toArray();
        $count = \ViewPgContact::count(array(
            "conditions"=>$conditions,
            "bind"=>array("search"=>"%".$search."%")
        ));
        $total = \ViewPgContact::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$data
        );
        echo json_encode($results);
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function editAction()
    {

        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \PgContact::findFirstById($id);
        if($id=="0") {
            $result = array();
            echo json_encode($result);
        }else{
            if ($data) {
                $result = $data->toArray();
                $listConfigRel = \PgRelContactGroup::find(array(
                    "conditions" => "id_contact=:id_contact:",
                    "bind" => array("id_contact" => $id)
                ));
                foreach ($listConfigRel as $lcr) {
                    $result['group_' . $lcr->id_group] = 1;
                }

                echo json_encode($result);
            }
            else
                echo json_encode(array());
        }
    }


    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = generateUuidString();
            $username = $this->session->username_admin;
            $data = new \PgContact();
            $data->assign($_POST);
            $data->id = $id;
            $data->save();
			
            foreach($_POST as $key => $value)
            {
                if(startsWith($key,"group_"))
                {
                    $configRel = new \PgRelContactGroup();
                    $configRel->id = generateUuidString();
                    $configRel->id_contact = $id;
                    $configRel->id_group = str_replace("group_","",$key);
                    $configRel->save();
                }
            }

            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $username = $this->session->username_admin;
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgContact::findFirstById($id);
            $data->assign($_POST);
            $data->update();

            $sql = "DELETE FROM PgRelContactGroup WHERE id_contact='".$id."'";
            $this->modelsManager->executeQuery($sql);

            foreach($_POST as $key => $value)
            {
                if(startsWith($key,"group_"))
                {
                    $configRel = new \PgRelContactGroup();
                    $configRel->id = generateUuidString();
                    $configRel->id_contact = $id;
                    $configRel->id_group = str_replace("group_","",$key);
                    $configRel->save();
                }
            }


            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $sql = "DELETE FROM PgRelContactGroup WHERE id_contact='".$id."'";
            $this->modelsManager->executeQuery($sql);
            $data = \PgContact::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
}