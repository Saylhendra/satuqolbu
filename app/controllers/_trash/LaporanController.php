<?php
/**
 * Created by PhpStorm.
 * User: IBM
 * Date: 18/08/2015
 * Time: 21:35
 */

namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class LaporanController extends AdminControllerBase
{
    private $TITLE = "Laporan";
    private $URL = "laporan";

    public function indexAction(){
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);
    }

    public function listAction()
    {
        \LoggerLibrary::logDebug("=====================listAction=================");
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $fromTgl = isset($_GET['fromTgl'])?$_GET['fromTgl']:date('d/m/Y');
        $fromTgl .= " 00:00:00";
        $toTgl = isset($_GET['toTgl'])?$_GET['toTgl']:date('d/m/Y');
        $toTgl .= " 23:59:00";
        $search = isset($_GET['search'])?$_GET['search']['value']:"";
//        $conditions = "name like :search: or notes like :search: LIMIT ".$length." OFFSET ".$start;
        $conditions = "id_category=:idCat: and time_reported > :timeMin: and time_reported < :timeMax: and id_regency=:idRegency:";
//        $conditions = "id_category=:idCat: and id_regency=:idRegency:";
        $datas = \PgCategoryComplaint::find();
        $count = \PgCategoryComplaint::count();
        $total = \PgCategoryComplaint::count();

        $list = array();
        foreach($datas as $data){
//            $complane = 0;
//            \LoggerLibrary::logDebug("============== from tgl ====================");
//            \LoggerLibrary::logDebug($fromTgl);
//            \LoggerLibrary::logDebug(strtotime($fromTgl));
//            \LoggerLibrary::logDebug(strtotime('+1 day', strtotime($fromTgl)));
//            \LoggerLibrary::logDebug("============== end from tgl ====================");
//            \LoggerLibrary::logDebug("================= complaint ==================");
            $complaint1 = \PgComplaint::count(array(
                 $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3173'
                )
            ));
            $complaint2 = \PgComplaint::count(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3172'
                )
            ));
//            \LoggerLibrary::logDebug($complaint2);
            $complaint3 = \PgComplaint::count(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3171'
                )
            ));
//            \LoggerLibrary::logDebug($complaint3);
            $complaint4 = \PgComplaint::count(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3174'
                )
            ));
//            \LoggerLibrary::logDebug($complaint4);
            $complaint5 = \PgComplaint::count(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3175'
                )
            ));
//            \LoggerLibrary::logDebug($complaint5);
//            $complaint1=0;$complaint2=0;$complaint3=0;$complaint4=0;$complaint5=0;

//            $complane = \PgComplaint::findByIdCategory($data->id_category);
            $list[] = array(
                'id' => $data->id,
                'category'=>$data->name,
                'jakpus' => $complaint1,
                'jaktim' => $complaint2,
                'jaksel' => $complaint3,
                'jakbar' => $complaint4,
                'jakut' => $complaint5
            );
        }


        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    public function laporanResponAction(){
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/laporan_respon',$model);
    }

    public function listLaporanResponAction(){
        $response = new \ResponseObject();
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $fromTgl = isset($_GET['fromTgl'])?$_GET['fromTgl']:date('d/m/Y');
        $fromTgl .= " 00:00:00";
        $toTgl = isset($_GET['toTgl'])?$_GET['toTgl']:date('d/m/Y');
        $toTgl .= " 23:59:00";

        $conditions = "time_reported > :timeMin: and time_reported < :timeMax:";
        $listData = \PgComplaint::find(array(
            $conditions,
            'bind'=>array(
                'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl))
            )
        ));

        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $img='';
            if($data->photo_complaint!=''){
                $img=base_url($data->photo_complaint);
            }else{
                if($data->photo_complaint_social!=''){
                    $img=$data->photo_complaint_social;
                }
            }
            $list[$idx] = $data->toArray();
            $list[$idx]["name_wil"] = \Safe::make($data)->regency->nama->safe_get('');
            $list[$idx]["name_category"] = \Safe::make($data)->category->name->safe_get('');
            $list[$idx]["source"] = '<img src="'.base_url('/public/'.$data->PgSource->img).'" width="32">';
            $list[$idx]["label_status"] = \ComplaintStatusPriority::getStatusPriority(\Safe::make($data)->status_priority->safe_get(0));
            $list[$idx]["photo_complaint"]=$img;
            $idx++;
        }
//        $count = \PgComplaint::count(array(
//            "conditions"=>$conditions,
//            "bind"=>array("search"=>"%".$search."%")
//        ));
//        $count = count($listData);
//        $total = \PgComplaint::count();

//        $results = array(
//            "draw" => $draw,
//            "recordsTotal" => $total,
//            "recordsFiltered" => $count,
//            "data"=>$list
//        );
        $response->isSuccess = false;
        $response->data = $list;
        if(count($list)){
            $response->isSuccess = true;
        }

        echo json_encode($response);

    }


    public function laporanRekapResponAction(){
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/laporan_rakap_respon',$model);
    }

    public function listLaporanRekapResponAction()
    {

        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $fromTgl = isset($_GET['fromTgl'])?$_GET['fromTgl']:date('d/m/Y');
        $fromTgl .= " 00:00:00";
        $toTgl = isset($_GET['toTgl'])?$_GET['toTgl']:date('d/m/Y');
        $toTgl .= " 23:59:59";
        $search = isset($_GET['search'])?$_GET['search']['value']:"";
//        $conditions = "name like :search: or notes like :search: LIMIT ".$length." OFFSET ".$start;
        $conditions = "id_category=:idCat: and time_reported > :timeMin: and time_reported < :timeMax: and id_regency=:idRegency:";
//        $conditions = "id_category=:idCat: and id_regency=:idRegency:";
        $datas = \PgCategoryComplaint::find();
        $count = \PgCategoryComplaint::count();
        $total = \PgCategoryComplaint::count();

        $list = array();
        foreach($datas as $data){
            $complaint1 = \PgComplaint::find(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3173'
                )
            ));
            $complaint1A=0; $complaint1B=0; $complaint1C=0; $complaint1D=0;
            foreach($complaint1 as $complaint){
                if($complaint->status_process=\ComplaintStatus::$MENUNGGU){   // status menunggu

                }elseif($complaint->status_process=\ComplaintStatus::$DITERIMA){  // status diterima
                    $complaint1A++;
                }elseif($complaint->status_process=\ComplaintStatus::$DIPROSES){  // diproses
                    $complaint1B++;

                    $complaint1A++;
                }elseif($complaint->status_process=\ComplaintStatus::$TIDAK_DIPROSES){  // tidak ditindak lanjuti
                    $complaint1C++;

                    $complaint1A++;
                    $complaint1B++;
                }elseif($complaint->status_process=\ComplaintStatus::$SELESAI){  //selesai
                    $complaint1D++;

                    $complaint1A++;
                    $complaint1B++;
                }
            }

            $complaint2 = \PgComplaint::find(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3172'
                )
            ));
            $complaint2A=0; $complaint2B=0; $complaint2C=0; $complaint2D=0;
            foreach($complaint2 as $complaint){
                if($complaint->status_process=\ComplaintStatus::$MENUNGGU){   // status menunggu

                }elseif($complaint->status_process=\ComplaintStatus::$DITERIMA){  // status diterima
                    $complaint2A++;
                }elseif($complaint->status_process=\ComplaintStatus::$DIPROSES){  // diproses
                    $complaint2B++;

                    $complaint2A++;
                }elseif($complaint->status_process=\ComplaintStatus::$TIDAK_DIPROSES){  // tidak ditindak lanjuti
                    $complaint2C++;

                    $complaint2A++;
                    $complaint2B++;
                }elseif($complaint->status_process=\ComplaintStatus::$SELESAI){  //selesai
                    $complaint2D++;

                    $complaint2A++;
                    $complaint2B++;
                }
            }

            $complaint3 = \PgComplaint::find(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3171'
                )
            ));
            $complaint3A=0; $complaint3B=0; $complaint3C=0; $complaint3D=0;
            foreach($complaint3 as $complaint){
                if($complaint->status_process=\ComplaintStatus::$MENUNGGU){   // status menunggu

                }elseif($complaint->status_process=\ComplaintStatus::$DITERIMA){  // status diterima
                    $complaint3A++;
                }elseif($complaint->status_process=\ComplaintStatus::$DIPROSES){  // diproses
                    $complaint3B++;

                    $complaint3A++;
                }elseif($complaint->status_process=\ComplaintStatus::$TIDAK_DIPROSES){  // tidak ditindak lanjuti
                    $complaint3C++;

                    $complaint3A++;
                    $complaint3B++;
                }elseif($complaint->status_process=\ComplaintStatus::$SELESAI){  //selesai
                    $complaint3D++;

                    $complaint3A++;
                    $complaint3B++;
                }
            }

            $complaint4 = \PgComplaint::find(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3174'
                )
            ));
            $complaint4A=0; $complaint4B=0; $complaint4C=0; $complaint4D=0;
            foreach($complaint4 as $complaint){
                if($complaint->status_process=\ComplaintStatus::$MENUNGGU){

                }elseif($complaint->status_process=\ComplaintStatus::$DITERIMA){
                    $complaint4A++;
                }elseif($complaint->status_process=\ComplaintStatus::$DIPROSES){
                    $complaint4B++;

                    $complaint4A++;
                }elseif($complaint->status_process=\ComplaintStatus::$TIDAK_DIPROSES){
                    $complaint4C++;

                    $complaint4A++;
                    $complaint4B++;
                }elseif($complaint->status_process=\ComplaintStatus::$SELESAI){
                    $complaint4D++;

                    $complaint4A++;
                    $complaint4B++;
                }
            }

            $complaint5 = \PgComplaint::find(array(
                $conditions,
                'bind' => array(
                    'idCat'=>$data->id,
                    'timeMin' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($fromTgl)),
                    'timeMax' => strtotime(\DateTimeLibrary::parseDateTimeIOtoSQL($toTgl)),
                    'idRegency' =>'3175'
                )
            ));
            $complaint5A=0; $complaint5B=0; $complaint5C=0; $complaint5D=0;
            foreach($complaint5 as $complaint){
                if($complaint->status_process=\ComplaintStatus::$MENUNGGU){

                }elseif($complaint->status_process=\ComplaintStatus::$DITERIMA){
                    $complaint5A++;
                }elseif($complaint->status_process=\ComplaintStatus::$DIPROSES){
                    $complaint5B++;

                    $complaint5A++;
                }elseif($complaint->status_process=\ComplaintStatus::$TIDAK_DIPROSES){
                    $complaint5C++;

                    $complaint5A++;
                    $complaint5B++;
                }elseif($complaint->status_process=\ComplaintStatus::$SELESAI){
                    $complaint5D++;

                    $complaint5A++;
                    $complaint5B++;
                }
            }

            $list[] = array(
                'id' => $data->id,
                'category'=>$data->name,

                'jakpusA' => $complaint1A,
                'jakpusB' => $complaint1B,
                'jakpusC' => $complaint1C,
                'jakpusD' => $complaint1D,

                'jaktimA' => $complaint2A,
                'jaktimB' => $complaint2B,
                'jaktimC' => $complaint2C,
                'jaktimD' => $complaint2D,

                'jakselA' => $complaint3A,
                'jakselB' => $complaint3B,
                'jakselC' => $complaint3C,
                'jakselD' => $complaint3D,

                'jakbarA' => $complaint4A,
                'jakbarB' => $complaint4B,
                'jakbarC' => $complaint4C,
                'jakbarD' => $complaint4D,

                'jakutA' => $complaint5A,
                'jakutB' => $complaint5B,
                'jakutC' => $complaint5C,
                'jakutD' => $complaint5D
            );
        }


        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }



}