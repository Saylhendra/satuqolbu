<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class TestController extends AdminControllerBase
{

    private $MODEL = "PgAdmin";
    private $TITLE = "Data Admin";
    private $URL = "test";


    public function testAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/test',$model);//,$arrPersonnel);
    }
}