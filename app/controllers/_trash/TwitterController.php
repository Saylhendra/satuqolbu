<?php
/**
 * Created by PhpStorm.
 * User: IBM
 * Date: 12/08/2015
 * Time: 20:36
 */

namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;



class TwitterController extends AdminControllerBase
{
    public function homeTimelineAction(){
        $home = \TwitterLibrary::getHomeTimeline(array('count'=>15));
        echo json_encode($home);
    }
    public function userTimelineAction(){
        $data = \TwitterLibrary::getUserTimeline(array('count'=>15));
        echo json_encode($data);
    }
    public function getStatusAction(){
        $id = $this->request->getQuery('id');
        $home = \TwitterLibrary::getStatusById($id);
        echo json_encode($home);
    }
    public function getMentionTimeLineAction(){
        $data = \TwitterLibrary::getMentions();
        echo json_encode($data);
    }
    public function getListsAction(){
        $data = \TwitterLibrary::getLists();
        echo json_encode($data);
    }
    public function getHomeNMentionsAction(){
        $home = \TwitterLibrary::getHomeTimeline(array('count'=>10));
        $mention = \TwitterLibrary::getMentions(array('count'=>10));
        $data = array_merge($home,$mention);
        echo json_encode($data);
    }
}