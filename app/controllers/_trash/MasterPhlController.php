<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MasterPhlController extends AdminControllerBase
{

    private $MODEL = "PgPhl";
    private $TITLE = "Master Pekerja Harian Lepas";
    private $URL = "master_phl";


    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }
	
	public function listAction()
	{
		$start = isset($_GET['start'])?$_GET['start']:0;
		$length = isset($_GET['length'])?$_GET['length']:10;
		$draw = isset($_GET['draw'])?$_GET['draw']:0;
		$search = isset($_GET['search'])?$_GET['search']['value']:"";
		$conditions = "username like :search: or nama like :search: or nip like :search: or email like :search: or keterangan like :search:  LIMIT ".$length." OFFSET ".$start;
		$data = \PgPhl::find(array(
					"conditions"=>$conditions,
					"bind"=>array("search"=>"%".$search."%")
				))->toArray();
		$count = \PgPhl::count(array(
					"conditions"=>$conditions,
					"bind"=>array("search"=>"%".$search."%")
				));
		$total = \PgPhl::count();

		$results = array(
            "draw" => $draw,
			"recordsTotal" => $total,
			"recordsFiltered" => $count,
			"data"=>$data
		);
		echo json_encode($results);
	}

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

	public function editAction()
	{

        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
	}

    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \PgPhl::findFirstById($id);
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \PgPhl();
            $data->assign($_POST);
            $data->password = md5($data->password);
            $data->id = $this->uuidString();
            $data->time_created = date("Y-m-d H:i:s");
            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgPhl::findFirstById($id);
            $pass = $data->password;
            $data->assign($_POST);
            if($pass == md5($_POST['password'])){
                $data->password = $pass;
            }
            else{
                $data->password = md5($_POST['password']);
            }
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgPhl::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
}