<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MapController extends AdminControllerBase
{

    private $MODEL = "PgComplaint";
    private $TITLE = "Map Pengaduan Masyarakat";
    private $URL = "map";
    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }
    public function pinAction(){
        $query=\PgComplaint::find();
        $data=array();
        foreach ($query as $row) {
            $data[]=array("id"=>$row->id,"x_coord"=>$row->latitude,"y_coord"=>$row->longitude);
        }
        echo json_encode($data);
    }
    public function detailAction(){
        $id=$_POST['id'];
        $query=\PgComplaint::findFirstById($id);
        $imgComplaint='';
        $imgSocial='';
        if($query->photo_complaint!=''){
            $imgComplaint='<img src="'.base_url('/public/'.$query->photo_complaint).'" width="150"/>';
        }
        if($query->photo_complaint_social!=''){
            $imgSocial='<img src="'.$query->photo_complaint_social.'" width="150"/>';
        }
        $data[]=array(
            "location"=>$query->location,
            "name_informer"=>$query->name_informer,
            "description"=>$query->description,
            "source"=>'<img src="'.base_url('/public/'.$query->PgSource->img).'" width="32">',
            "name_informer"=>$query->name_informer,
            "photoComplaint"=>$imgComplaint,
            "photoSocial"=>$imgSocial,
            "time_reported"=>date('d/m/Y H:i:s',$query->time_reported),
            "category"=>$query->category->name
            );
        echo json_encode($data);

    }
    
}