<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class SmsController extends AdminControllerBase
{

    private $MODEL = "Inbox";
    private $TITLE = "Input Pengaduan Lewat SMS";
    private $URL = "sms";

    public function indexAction()
    {
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $this->view->partial('admin/'.$this->URL.'/index',$model);//,$arrPersonnel);
    }

    public function listAction()
    {
        $start = isset($_GET['start'])?$_GET['start']:0;
        $length = isset($_GET['length'])?$_GET['length']:10;
        $draw = isset($_GET['draw'])?$_GET['draw']:0;
        $search = isset($_GET['search'])?$_GET['search']['value']:"";

        $conditions = "description like :search: or
                name_informer like :search: or
                    notes_proceed like :search: or
                        description like :search:
                                LIMIT ".$length." OFFSET ".$start;

        $listData = \PgComplaint::find(array(
            "conditions"=>$conditions,
            "bind"=>array("search"=>"%".$search."%")
        ));
        $list = array();
        $idx = 0;
        foreach($listData as $data){
            $list[$idx] = $data->toArray();
            $list[$idx]["name_category"] = \Safe::make($data)->category->name->safe_get('');
            $list[$idx]["label_status"] = \ComplaintStatus::getStatus(\Safe::make($data)->status->safe_get(0));
            $idx++;
        }
        $count = \PgComplaint::count(array(
            "conditions"=>$conditions,
            "bind"=>array("search"=>"%".$search."%")
        ));
        $total = \PgComplaint::count();

        $results = array(
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $count,
            "data"=>$list
        );
        echo json_encode($results);
    }

    //Untuk Fungsi di Form
    public function getAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $data = \PgComplaint::findFirstById($id);
        if($data)
            echo json_encode($data->toArray());
        else
            echo json_encode(array());
    }

    public function newAction()
    {
        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "save";
        $model['labelSubmit'] = "Save";
        $model['labelForm'] = "Create";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function editAction()
    {

        $id = isset($_GET['id'])?$_GET['id']:"0";
        $model = array();
        $model['id'] = $id;
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = "update";
        $model['labelSubmit'] = "Update";
        $model['labelForm'] = "Edit";
        $this->view->partial('admin/'.$this->URL.'/form',$model);
    }

    public function saveAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $data = new \PgComplaint();
            $data->assign($_POST);
            $data->id = $this->uuidString();
            $data->time_created = date("Y-m-d H:i:s");
            $data->save();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function updateAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgComplaint::findFirstById($id);
            $data->assign($_POST);
            $data->update();
            $response->message = \T::message("all.label.message.success_save");
            $this->db->commit();

        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

    public function removeAction()
    {
        try {
            $response = new \ResponseObject();
            $this->db->begin();
            $id = $this->request->getPost("id");
            $data = \PgComplaint::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }
}

