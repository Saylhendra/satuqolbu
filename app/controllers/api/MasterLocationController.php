<?php
namespace ApplicationApi;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MasterLocationController extends \ControllerBase
{

    public function indexAction(){
        echo "test mlc index";
    }

    public function get_current_locationAction()
    {
        $result = \CommonLibrary::getRequestGeoInfo();
        echo json_encode($result);
    }
    public function get_countryAction()
    {
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $conditions = "1=1 ";
        $bind = array();
        if(isset($id)){
            $conditions .= " and id = :id:";
            $bind['id'] = $id;
        }
        if(isset($ids)){
            $conditions .= " and id in (".$ids.")";
            $bind['ids'] = $ids;
        }
        echo json_encode(\KdgMstCountry::find(array(
            "conditions" => $conditions,
            "bind"       => $bind
        ))->toArray());
    }


    public function get_provinceAction()
    {
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_country = $this->request->getQuery('id_country');
        $id_countries = $this->request->getQuery('id_countries');

        $conditions = "1=1 ";
        $bind = array();
        if(isset($id)){
            $conditions .= " and id = :id:";
            $bind['id'] = $id;
        }
        if(isset($ids)){
            $conditions .= " and id in (".$ids.")";
            $bind['ids'] = $ids;
        }
        if(isset($id_country)){
            $conditions .= " and id_country = :id_country:";
            $bind['id_country'] = $id_country;
        }
        if(isset($id_countries)){
            $conditions .= " and id_country in (".$id_countries.")";
            $bind['id_countries'] = $id_countries;
        }
        echo json_encode(\KdgMstProvince::find(array(
            "conditions" => $conditions,
            "bind"       => $bind
        ))->toArray());
    }


    public function get_regencyAction()
    {
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_province = $this->request->getQuery('id_province');
        $id_provinces = $this->request->getQuery('id_provinces');

        $conditions = "1=1 ";
        $bind = array();
        if(isset($id)){
            $conditions .= " and id = :id:";
            $bind['id'] = $id;
        }
        if(isset($ids)){
            $conditions .= " and id in (".$ids.")";
            $bind['ids'] = $ids;
        }
        if(isset($id_province)){
            $conditions .= " and id_province = :id_province:";
            $bind['id_province'] = $id_province;
        }
        if(isset($id_provinces)){
            $conditions .= " and id_province in (".$id_provinces.")";
            $bind['id_provinces'] = $id_provinces;
        }
        echo json_encode(\KdgMstRegency::find(array(
            "conditions" => $conditions,
            "bind"       => $bind
        ))->toArray());
    }

    public function get_sub_districtAction()
    {
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_regency = $this->request->getQuery('id_regency');
        $id_regencies = $this->request->getQuery('id_regencies');

        $conditions = "1=1 ";
        $bind = array();
        if(isset($id)){
            $conditions .= " and id = :id:";
            $bind['id'] = $id;
        }
        if(isset($ids)){
            $conditions .= " and id in (".$ids.")";
            $bind['ids'] = $ids;
        }
        if(isset($id_regency)){
            $conditions .= " and id_regency = :id_regency:";
            $bind['id_regency'] = $id_regency;
        }
        if(isset($id_regencies)){
            $conditions .= " and id_regency in (".$id_regencies.")";
            $bind['id_regencies'] = $id_regencies;
        }
        echo json_encode(\KdgMstSubDistrict::find(array(
            "conditions" => $conditions,
            "bind"       => $bind
        ))->toArray());
    }
}