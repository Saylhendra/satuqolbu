<?php
namespace ApplicationApi;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MasterProductController extends \ControllerBase{
    public function get_product(){
        $id_business=$_get['id_business'];
        \LoggerLibrary::logDebug("test api produk");
        \LoggerLibrary::logDebug($id_business);
        $conditions = "1=1 ";
        $bind = array();

//        if($id_business) {
            $conditions .= " and id_business = :id_business:";
            $bind['id_business'] = $id_business;
//        }

        echo json_encode(\KdgMstProduct::find(array(
            "conditions" => $conditions,
            "bind"       => $bind
        ))->toArray());
    }

}

?>