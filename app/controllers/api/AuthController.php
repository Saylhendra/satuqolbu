<?php
namespace ApplicationApi;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class AuthController extends \ControllerBase
{

    public function indexAction(){
        echo "test mlc index";
    }

    public function loginAction()
    {
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        $result = \MemberService::loginMember($username,$password);
        echo json_encode($result);
    }
}