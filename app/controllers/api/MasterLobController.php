<?php
namespace ApplicationApi;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class MasterLobController extends \ControllerBase
{

    public function indexAction(){
        echo "test mlc index";
    }

    public function get_lobAction()
    {
        $id = $this->request->getQuery('id');
        $ids = $this->request->getQuery('ids');
        $id_parent = $this->request->getQuery('id_parent');
        $id_parents = $this->request->getQuery('id_parents');
        $level1 = $this->request->getQuery('level1');
        $level1s = $this->request->getQuery('level1s');
        $level2 = $this->request->getQuery('level2');
        $level2s = $this->request->getQuery('level2s');
        $level3 = $this->request->getQuery('level3');
        $level3s = $this->request->getQuery('level3s');
        $level4 = $this->request->getQuery('level4');
        $level4s = $this->request->getQuery('level4s');
        $code = $this->request->getQuery('code');
    //    $codes = $request->getQuery('codes');
        $code_level = $this->request->getQuery('code_level');
        $code_levels = $this->request->getQuery('code_levels');
        $conditions = "1=1 ";
        $bind = array();
        $conditions .= " and lang = :lang:";
        $bind['lang'] = "en";
        if(isset($id)){
            $conditions .= " and id = :id:";
            $bind['id'] = $id;
        }
        if(isset($ids)){
            $conditions .= " and en in (".$ids.")";
            $bind['ids'] = $ids;
        }
        if(isset($id_parent)){
            $conditions .= " and id_parent = :id_parent:";
            $bind['id_parent'] = $id_parent;
        }
        if(isset($id_parents)){
            $conditions .= " and id_parent in (".$id_parents.")";
            $bind['id_parents'] = $id_parents;
        }
        if(isset($level1)){
            $conditions .= " and level1 = :level1:";
            $bind['level1'] = $level1;
        }
        if(isset($level1s)){
            $conditions .= " and level1 in (".$level1s.")";
            $bind['level1s'] = $level1s;
        }
        if(isset($level2)){
            $conditions .= " and level2 = :level2:";
            $bind['level2'] = $level2;
        }
        if(isset($level2s)){
            $conditions .= " and level2 in (".$level2s.")";
            $bind['level2s'] = $level2s;
        }
        if(isset($level3)){
            $conditions .= " and level3 = :level3:";
            $bind['level3'] = $level3;
        }
        if(isset($level3s)){
            $conditions .= " and level3 in (".$level3s.")";
            $bind['level3s'] = $level3s;
        }
        if(isset($level4)){
            $conditions .= " and level4 = :level4:";
            $bind['level4'] = $level4;
        }
        if(isset($level4s)){
            $conditions .= " and level4 in (".$level4s.")";
            $bind['level4s'] = $level4s;
        }
        if(isset($code_level)){
            $conditions .= " and code_level = :code_level:";
            $bind['code_level'] = $code_level;
        }
        if(isset($code_levels)){
            $conditions .= " and code_level in (".$code_levels.")";
            $bind['code_levels'] = $code_levels;
        }
        if(isset($code)){
            $conditions .= " and code like :code:";
            $bind['code'] = $code."%";
        }
        
        echo json_encode(\KdgMstLob::find(array(
            "conditions" => $conditions,
            "bind"       => $bind
        ))->toArray());
    }

   

}