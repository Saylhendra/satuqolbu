<?php




class PgRelContactGroup extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $id_contact;
     
    /**
     *
     * @var string
     */
    public $id_group;

    public function getSource()
    {
        return "pg_rel_contact_group";
    }
     
}
