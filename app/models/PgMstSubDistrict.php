<?php




class PgMstSubDistrict extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $id_regency;
     
    /**
     *
     * @var string
     */
    public $nama;

    public function getSource()
    {
        return "pg_mst_sub_district";
    }
     
}
