<?php




class PgVariable extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $value;
     
    /**
     *
     * @var string
     */
    public $notes;

    public function getSource()
    {
        return "pg_variable";
    }
     
}
