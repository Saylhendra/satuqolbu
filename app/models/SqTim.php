<?php

class SqTim extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_pimpinan;
    public $nama;
    public $tgl_bentuk;
    public $tgl_berakhir;
    public $ket;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;
    public $created_date;
    public $update_date;
    public $tipe;

    public function initialize()
    {
        $this->belongsTo('id_pimpinan', 'SqCustomer', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_pimpinan' => 'id_pimpinan', 
            'nama' => 'nama', 
            'tgl_bentuk' => 'tgl_bentuk', 
            'tgl_berakhir' => 'tgl_berakhir',
            'ket' => 'ket',
            'path_original' => 'path_original',
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'tipe' => 'tipe'
        );
    }

    public function getCustomer(){
        return \SqCustomer::findFirstById($this->id_pimpinan);
    }

}
