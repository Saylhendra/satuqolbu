<?php

class SqBtqGroup extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
    public $id_peserta;
    public $id_mentor;
    public $nama;
    public $created_date;
    public $update_date;
    public $status;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;

    public function initialize()
    {
        $this->belongsTo('id_mentor', 'SqBtqMentor', 'id', NULL);
        $this->belongsTo('id_peserta', 'SqBtqPeserta', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_peserta' => 'id_peserta', 
            'id_mentor' => 'id_mentor', 
            'nama' => 'nama', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status', 
            'path_original' => 'path_original', 
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails'
        );
    }

}
