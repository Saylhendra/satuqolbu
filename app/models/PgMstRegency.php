<?php




class PgMstRegency extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $id_province;
     
    /**
     *
     * @var string
     */
    public $nama;

    public function getSource()
    {
        return "pg_mst_regency";
    }
     
}
