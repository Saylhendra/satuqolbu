<?php

class SqGallery extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_group;
    public $nama;
    public $subtitle;
    public $deskripsi;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;
    public $created_date;
    public $update_date;
    public $status;

    public function initialize()
    {
        $this->belongsTo('id_group', 'SqMstGroup', 'id', array('alias' => 'mstGroup'));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_group' => 'id_group', 
            'nama' => 'nama', 
            'subtitle' => 'subtitle', 
            'deskripsi' => 'deskripsi', 
            'path_original' => 'path_original', 
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status'
        );
    }

}
