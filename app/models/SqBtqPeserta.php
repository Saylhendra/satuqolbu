<?php

class SqBtqPeserta extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_customer;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $created_date;

    /**
     *
     * @var string
     */
    public $update_date;

    /**
     *
     * @var integer
     */
    public $status;

    public function initialize()
    {
        $this->belongsTo('id_customer', 'SqCustomer', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_customer' => 'id_customer', 
            'nama' => 'nama', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status'
        );
    }

}
