<?php


class PgSource extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $value;
     
    /**
     *
     * @var string
     */
    public $text;
     
    /**
     *
     * @var string
     */
    public $img;

    public function getSource()
    {
        return "pg_source";
    }
     
   

}
