<?php

class SqTimAnggota extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_anggota;
    public $nama_anggota;
    public $id_tim;
    public $nama_tim;
    public $mobile;
    public $alamat;
    public $ket;
    public $created_date;
    public $update_date;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;

    public function initialize()
    {
        $this->belongsTo('id_anggota', 'SqCustomer', 'id', NULL);
        $this->belongsTo('id_tim', 'SqTim', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_anggota' => 'id_anggota', 
            'nama_anggota' => 'nama_anggota',
            'id_tim' => 'id_tim',
            'nama_tim' => 'nama_tim',
            'ket' => 'ket',
            'mobile' => 'mobile',
            'alamat' => 'alamat',
            'created_date' => 'created_date',
            'update_date' => 'update_date',
            'path_original' => 'path_original',
            'path_large' => 'path_large',
            'path_medium' => 'path_medium',
            'path_small' => 'path_small',
            'path_thumbnails' => 'path_thumbnails'
        );
    }

}
