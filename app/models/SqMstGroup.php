<?php

class SqMstGroup extends \Phalcon\Mvc\Model
{

    public $id;
    public $nama;
    public $created_date;
    public $update_date;
    public $status;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status'
        );
    }

}
