<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class SqMember extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $id_sekolah;

    /**
     *
     * @var string
     */
    public $id_mr;

    /**
     *
     * @var string
     */
    public $id_tim;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var integer
     */
    public $jns_kelamin;

    /**
     *
     * @var integer
     */
    public $gol_darah;

    /**
     *
     * @var string
     */
    public $skill_khusus;

    /**
     *
     * @var string
     */
    public $tempat_lahir;

    /**
     *
     * @var string
     */
    public $tgl_lahir;

    /**
     *
     * @var string
     */
    public $pekerjaan;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $mobile;

    /**
     *
     * @var integer
     */
    public $sts_nikah;

    /**
     *
     * @var string
     */
    public $alamat;

    /**
     *
     * @var integer
     */
    public $is_membina;

    /**
     *
     * @var string
     */
    public $id_division;

    /**
     *
     * @var string
     */
    public $created_date;

    /**
     *
     * @var string
     */
    public $update_date;

    /**
     *
     * @var integer
     */
    public $status_penugasan;

    /**
     *
     * @var string
     */
    public $ket;

    /**
     *
     * @var string
     */
    public $path_original;

    /**
     *
     * @var string
     */
    public $path_large;

    /**
     *
     * @var string
     */
    public $path_medium;

    /**
     *
     * @var string
     */
    public $path_small;

    /**
     *
     * @var string
     */
    public $path_thumbnails;

    /**
     *
     * @var integer
     */
    public $tipe;

    /**
     *
     * @var integer
     */
    public $jabatan1;

    /**
     *
     * @var integer
     */
    public $jabatan2;

    /**
     *
     * @var integer
     */
    public $jabatan3;

    /**
     *
     * @var integer
     */
    public $jabatan4;

    /**
     *
     * @var integer
     */
    public $jabatan5;

    /**
     *
     * @var integer
     */
    public $sts_aktif;

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_sekolah' => 'id_sekolah', 
            'id_mr' => 'id_mr', 
            'id_tim' => 'id_tim', 
            'first_name' => 'first_name', 
            'last_name' => 'last_name', 
            'jns_kelamin' => 'jns_kelamin', 
            'gol_darah' => 'gol_darah', 
            'skill_khusus' => 'skill_khusus', 
            'tempat_lahir' => 'tempat_lahir', 
            'tgl_lahir' => 'tgl_lahir', 
            'pekerjaan' => 'pekerjaan', 
            'email' => 'email', 
            'mobile' => 'mobile', 
            'sts_nikah' => 'sts_nikah', 
            'alamat' => 'alamat', 
            'is_membina' => 'is_membina', 
            'id_division' => 'id_division', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status_penugasan' => 'status_penugasan', 
            'ket' => 'ket', 
            'path_original' => 'path_original', 
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails', 
            'tipe' => 'tipe', 
            'jabatan1' => 'jabatan1', 
            'jabatan2' => 'jabatan2', 
            'jabatan3' => 'jabatan3', 
            'jabatan4' => 'jabatan4', 
            'jabatan5' => 'jabatan5', 
            'sts_aktif' => 'sts_aktif'
        );
    }

}
