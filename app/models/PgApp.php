<?php




class PgApp extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $app_name;
     
    /**
     *
     * @var string
     */
    public $app_author;
     
    /**
     *
     * @var string
     */
    public $app_key;
    public function getSource()
    {
        return "pg_app";
    }
     
}
