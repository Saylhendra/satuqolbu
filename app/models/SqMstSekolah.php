<?php

class SqMstSekolah extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nm_sekolah;

    /**
     *
     * @var integer
     */
    public $sts_lahan_dkw;

    /**
     *
     * @var integer
     */
    public $sts_aktif_kelola;

    /**
     *
     * @var string
     */
    public $alamat;

    /**
     *
     * @var string
     */
    public $thn_gabung;

    /**
     *
     * @var string
     */
    public $path_original;

    /**
     *
     * @var string
     */
    public $path_large;

    /**
     *
     * @var string
     */
    public $path_medium;

    /**
     *
     * @var string
     */
    public $path_small;

    /**
     *
     * @var string
     */
    public $path_thumbnails;

    /**
     *
     * @var string
     */
    public $created_date;

    /**
     *
     * @var string
     */
    public $update_date;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nm_sekolah' => 'nm_sekolah', 
            'sts_lahan_dkw' => 'sts_lahan_dkw', 
            'sts_aktif_kelola' => 'sts_aktif_kelola', 
            'alamat' => 'alamat', 
            'thn_gabung' => 'thn_gabung', 
            'path_original' => 'path_original', 
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status'
        );
    }

}
