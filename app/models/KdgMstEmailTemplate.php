<?php




class KdgMstEmailTemplate extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $code;
     
    /**
     *
     * @var string
     */
    public $lang;
     
    /**
     *
     * @var string
     */
    public $subject;
     
    /**
     *
     * @var string
     */
    public $content;
     
    /**
     *
     * @var string
     */
    public $notes;
     
    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'code' => 'code', 
            'lang' => 'lang', 
            'subject' => 'subject', 
            'content' => 'content', 
            'notes' => 'notes'
        );
    }

}
