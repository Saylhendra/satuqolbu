<?php

class SqNotifikasi extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_member_creator;
    public $id_member_tujuan;
    public $type_data;
    public $type_action;
    public $created_date;
    public $update_date;
    public $status;

    public function initialize()
    {
        $this->belongsTo('id_member_creator', 'SqCustomer', 'id', array('alias' => 'memberCreator'));
        $this->belongsTo('id_member_tujuan', 'SqCustomer', 'id', array('alias' => 'memberTujuan'));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_member_creator' => 'id_member_creator', 
            'id_member_tujuan' => 'id_member_tujuan', 
            'type_data' => 'type_data', 
            'type_action' => 'type_action', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status'
        );
    }

}
