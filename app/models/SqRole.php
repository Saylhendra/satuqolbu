<?php

class SqRole extends \Phalcon\Mvc\Model
{

    public $id;
    public $nama;
    public $deskripsi;
    public $created_date;
    public $update_date;

    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama',
            'deskripsi' => 'deskripsi',
            'created_date' => 'created_date',
            'update_date' => 'update_date'
        );
    }

}
