<?php

class SqJurnal extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_kegiatan;
    public $tipe_kegiatan;
    public $lokasi;
    public $id_tim;
    public $id_pimpinan;
    public $is_pimpinan_hadir;
    public $tgl_kegiatan;
    public $nama;
    public $jml_peserta;
    public $catatan;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;
    public $created_date;
    public $update_date;
    public $status;

    public function initialize()
    {
        $this->belongsTo('id_kegiatan', 'SqMstKegiatan', 'id', NULL);
        $this->belongsTo('id_tim', 'SqTim', 'id', NULL);
        $this->belongsTo('id_pimpinan', 'SqCustomer', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_kegiatan' => 'id_kegiatan',
            'tipe_kegiatan' => 'tipe_kegiatan',
            'lokasi' => 'lokasi',
            'id_tim' => 'id_tim',
            'id_pimpinan' => 'id_pimpinan',
            'is_pimpinan_hadir' => 'is_pimpinan_hadir',
            'tgl_kegiatan' => 'tgl_kegiatan',
            'nama' => 'nama', 
            'jml_peserta' => 'jml_peserta', 
            'catatan' => 'catatan', 
            'path_original' => 'path_original', 
            'path_large' => 'path_large', 
            'path_medium' => 'path_medium', 
            'path_small' => 'path_small', 
            'path_thumbnails' => 'path_thumbnails', 
            'created_date' => 'created_date', 
            'update_date' => 'update_date', 
            'status' => 'status'
        );
    }

}
