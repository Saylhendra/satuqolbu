<?php




class ViewPgRelContactGroup extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $id_contact;
     
    /**
     *
     * @var string
     */
    public $id_group;
     
    /**
     *
     * @var string
     */
    public $name_group;
     
    /**
     *
     * @var string
     */
    public $name_contact;
     
    /**
     *
     * @var string
     */
    public $phone_contact;
     
    /**
     *
     * @var string
     */
    public $email_contact;

    public function getSource()
    {
        return "view_pg_rel_contact_group";
    }
}
