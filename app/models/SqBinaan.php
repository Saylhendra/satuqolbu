<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class SqBinaan extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_sekolah;
    public $first_name;
    public $last_name;
    public $jns_kelamin;
    public $gol_darah;
    public $skill_khusus;
    public $tempat_lahir;
    public $tgl_lahir;
    public $pekerjaan;
    public $email;
    public $mobile;
    public $sts_nikah;
    public $alamat;
    public $is_membina;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;
    public $created_date;
    public $update_date;
    public $status_penugasan;
    public $ket;
    public $path_file;
    public $id_mr;
    public $tipe;
    public $id_role;
    public $sts_aktif;

    public function initialize()
    {

        $this->belongsTo('id_sekolah', 'SqMstSekolah', 'id', NULL);
        $this->belongsTo('id_mr', 'SqCustomer', 'id', NULL);
        $this->belongsTo('id_role', 'SqRole', 'id', NULL);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'id_sekolah' => 'id_sekolah',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'jns_kelamin' => 'jns_kelamin',
            'gol_darah' => 'gol_darah',
            'skill_khusus' => 'skill_khusus',
            'tempat_lahir' => 'tempat_lahir',
            'tgl_lahir' => 'tgl_lahir',
            'pekerjaan' => 'pekerjaan',
            'email' => 'email',
            'mobile' => 'mobile',
            'sts_nikah' => 'sts_nikah',
            'alamat' => 'alamat',
            'is_membina' => 'is_membina',
            'path_original' => 'path_original',
            'path_large' => 'path_large',
            'path_medium' => 'path_medium',
            'path_small' => 'path_small',
            'path_thumbnails' => 'path_thumbnails',
            'created_date' => 'created_date',
            'update_date' => 'update_date',
            'status_penugasan' => 'status_penugasan',
            'ket' => 'ket',
            'path_file' => 'path_file',
            'id_mr' => 'id_mr',
            'tipe' => 'tipe',
            'id_role' => 'id_role',
            'sts_aktif' => 'sts_aktif'
        );
    }

    public function getMentor()
    {
        return \SqBtqMentor::findFirstByIdCustomer($this->id);
    }

}
