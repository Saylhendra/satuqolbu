<?php


use Phalcon\Mvc\Model\Validator\Email as Email;

class PgUserDivision extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $username;
     
    /**
     *
     * @var string
     */
    public $password;
     
    /**
     *
     * @var string
     */
    public $nama;
     
    /**
     *
     * @var string
     */
    public $nip;
     
    /**
     *
     * @var string
     */
    public $email;
     
    /**
     *
     * @var string
     */
    public $id_division;
     
    /**
     *
     * @var string
     */
    public $time_created;
     
    /**
     *
     * @var string
     */
    public $keterangan;
     
    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    "field"    => "email",
                    "required" => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    public function getSource()
    {
        return "pg_user_division";
    }

}
