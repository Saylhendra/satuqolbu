<?php

class SqMstKegiatan extends \Phalcon\Mvc\Model
{

    public $id;
    public $nama;
    public $sub_judul;
    public $tipe_pin;
    public $icon;
    public $link;
    public $notes;
    public $deskripsi;
    public $created_date;
    public $update_date;

    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'sub_judul' => 'sub_judul',
            'tipe_pin' => 'tipe_pin',
            'icon' => 'icon',
            'link' => 'link',
            'notes' => 'notes',
            'deskripsi' => 'deskripsi',
            'created_date' => 'created_date',
            'update_date' => 'update_date'
        );
    }

}
