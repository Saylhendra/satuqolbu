<?php

class PgComplaint extends \Phalcon\Mvc\Model
{

    public $id;
    public $photo_complaint; //diinput manual
    public $longitude;
    public $latitude;
    public $location;
    public $id_category;
    public $time_created;
    public $time_proceed;
    public $photo_proceed;
    public $notes_proceed; //nanti diisi phl
    public $time_solved;
    public $photo_solved;
    public $notes_solved;
    public $status_priority; //low high medium
    public $status_process; // status diproses 0 pending, 1
    public $status_respons_unit; // status tindak lanjut
    public $notes_mission; //catatan waktu awal diisi di awal
    public $tag; //tagging (text area)
    public $source; //untuk Twitter, Qlue, SMS (combobox, di manual ga ada pilihan, dan terset otomatis type manual)
    public $description; //ngambil dari twitter atau smsnya
    public $name_informer;
    public $id_fk;//untuk id Twitter, Qlue, SMS
    public $id_phl;
    public $time_reported;
    public $id_division; //low high medium
    public $id_regency;
    public $id_sub_district;
    public $photo_complaint_social;
    public $ket;


    public function initialize()
    {
        $this->belongsTo('id_category', 'PgCategoryComplaint', 'id', array("alias"=>"Category"));
        $this->belongsTo('id_regency', 'PgMstRegency', 'id', array("alias"=>"Regency"));
        $this->belongsTo('id_sub_district', 'PgMstSubDistrict', 'id', array("alias"=>"SubDistrict"));
        $this->belongsTo('id_division', 'PgDivision', 'id', array("alias"=>"Division"));
        $this->belongsTo('source', 'PgSource', 'value',null);
    }
    public function getSource()
    {
        return "pg_complaint";
    }
}
