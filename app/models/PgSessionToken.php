<?php




class PgSessionToken extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $id_app;
     
    /**
     *
     * @var string
     */
    public $id_member;
     
    /**
     *
     * @var string
     */
    public $session_key;
     
    /**
     *
     * @var string
     */
    public $device_id;
     
    /**
     *
     * @var string
     */
    public $os_type;
     
    /**
     *
     * @var string
     */
    public $created_time;
     
    /**
     *
     * @var string
     */
    public $session_log;

    public function getSource()
    {
        return "pg_session_token";
    }
     
}
