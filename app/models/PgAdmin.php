<?php


use Phalcon\Mvc\Model\Validator\Email as Email;

class PgAdmin extends \Phalcon\Mvc\Model
{

    public $id;
    public $username;
    public $password;
    public $nama;
    public $nip;
    public $email;
    public $time_created;
    public $keterangan;
    public $id_division;
    public $is_admin;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;
    public $created_date;
    public $update_date;
    public $id_customer;

    /**
     * Validations and business logic
     */
    public function initialize()
    {
        $this->belongsTo('id_division', 'PgDivision', 'id', array("alias"=>"Division"));
        $this->belongsTo('id_customer', 'SqCustomer', 'id');
    }
    public function getSource()
    {
        return "pg_admin";
    }
    public function getCustomer(){
        return \SqCustomer::findFirstByEmail($this->email);
    }
}