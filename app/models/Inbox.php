<?php




class Inbox extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $UpdatedInDB;
     
    /**
     *
     * @var string
     */
    public $ReceivingDateTime;
     
    /**
     *
     * @var string
     */
    public $Text;
     
    /**
     *
     * @var string
     */
    public $SenderNumber;
     
    /**
     *
     * @var string
     */
    public $Coding;
     
    /**
     *
     * @var string
     */
    public $UDH;
     
    /**
     *
     * @var string
     */
    public $SMSCNumber;
     
    /**
     *
     * @var integer
     */
    public $Class;
     
    /**
     *
     * @var string
     */
    public $TextDecoded;
     
    /**
     *
     * @var integer
     */
    public $ID;
     
    /**
     *
     * @var string
     */
    public $RecipientID;
     
    /**
     *
     * @var string
     */
    public $Processed;
     
}
