<?php

/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 2/9/2015
 * Time: 2:46 PM
 */
class HalaqohService extends ServiceBase
{
    public static function saveNewBinaan($dataForm, $uuid, $fileName){

        $response = new ResponseObject();
        try{
            $data = new \SqBinaan();
            $data->assign($dataForm);
            $data->id = $uuid;

            if (array_key_exists('detailfile', $fileName)) {
                $uploadimg = \UploadLibrary::upload_picture($fileName['detailfile']);

                if ($uploadimg['isSuccess']) {
                    $data->path_small = $uploadimg['path_small'];
                    $data->path_medium = $uploadimg['path_medium'];
                    $data->path_large = $uploadimg['path_large'];
                    $data->path_thumbnails = $uploadimg['path_thumbnails'];
                }
            }

            $data->tipe = \TypeCustomer::$BINAAN;
            $data->is_membina = \StatusMembina::$BELUM;
            /*=========================================*/
            $data->created_date = date('Y-m-d H:i:s');
            $data->update_date = date('Y-m-d H:i:s');

            $data->save();
            $response->isSuccess = true;
            $response->message = "Success";

        }catch(Exception $ex){
            $response->isSuccess = false;
            $response->message = $ex->getTraceAsString();
        }

        return $response;
    }

    public static function loginAdmin($username, $password)
    {
        $response = new ResponseObject();
        $response->isSuccess = TRUE;

        $user = \PgAdmin::findFirst(array(
            "conditions" => "username=:username: AND password=:password: ",
            "bind" => array(
                "username" => $username,
                "password" => md5($password)
            )
        ));

        if (!$user) {
            $response->isSuccess = FALSE;
            $response->message = T::message("login.message.error");
        } else {
            $response->message = T::message("login.message.success");
        }

        $response->data = $user;

        return $response;
    }
}