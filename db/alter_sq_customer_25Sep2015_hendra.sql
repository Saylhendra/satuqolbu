ALTER TABLE `sq_customer` ADD `gol_darah` TINYINT(4) NULL AFTER `jns_kelamin`;
ALTER TABLE `sq_customer` ADD `skill_khusus` VARCHAR(200) NULL AFTER `gol_darah`;

ALTER TABLE `sq_btq_mentor` ADD `hobi` VARCHAR(200) NULL AFTER `nama`;
ALTER TABLE `sq_btq_mentor` ADD `pendidikan` VARCHAR(200) NULL AFTER `hobi`;
ALTER TABLE `sq_btq_mentor` ADD `jurusan` VARCHAR(200) NULL AFTER `pendidikan`;
ALTER TABLE `sq_btq_mentor` ADD `peminatan` VARCHAR(200) NULL AFTER `jurusan`;