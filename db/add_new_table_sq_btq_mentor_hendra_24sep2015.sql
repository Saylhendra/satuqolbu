drop table if exists sq_btq_mentor;

/*==============================================================*/
/* Table: sq_btq_mentor                                         */
/*==============================================================*/
create table sq_btq_mentor
(
   id                   varchar(60) not null,
   id_customer          varchar(60),
   nama                 varchar(200),
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);