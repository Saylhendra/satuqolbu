ALTER TABLE `sq_tim` CHANGE `tgl_bentuk` `tgl_bentuk` DATE NULL DEFAULT NULL;
ALTER TABLE `sq_tim` ADD `tgl_berakhir` DATE NULL AFTER `tgl_bentuk`;
ALTER TABLE `sq_customer` CHANGE `status` `status_penugasan` TINYINT(4) NULL DEFAULT '0';