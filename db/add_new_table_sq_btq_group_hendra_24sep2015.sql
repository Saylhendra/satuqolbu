drop table if exists sq_btq_group;

/*==============================================================*/
/* Table: sq_btq_group                                          */
/*==============================================================*/
create table sq_btq_group
(
   id                   varchar(60) not null,
   id_peserta           varchar(60),
   id_mentor            varchar(60),
   nama                 varchar(200),
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   path_original        varchar(255),
   path_large           text,
   path_medium          mediumtext,
   path_small           mediumtext,
   path_thumbnails      text,
   primary key (id)
);