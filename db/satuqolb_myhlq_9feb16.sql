/*
Navicat MySQL Data Transfer

Source Server         : mysql_local
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : satuqolb_myhlq

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-02-09 00:35:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for kdg_mst_email_template
-- ----------------------------
DROP TABLE IF EXISTS `kdg_mst_email_template`;
CREATE TABLE `kdg_mst_email_template` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `subject` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci,
  `notes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for pg_admin
-- ----------------------------
DROP TABLE IF EXISTS `pg_admin`;
CREATE TABLE `pg_admin` (
  `id` varchar(60) NOT NULL,
  `id_customer` varchar(60) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL,
  `keterangan` text,
  `id_division` varchar(60) DEFAULT NULL,
  `is_admin` tinyint(4) DEFAULT NULL,
  `path_original` varchar(255) DEFAULT NULL,
  `path_large` text,
  `path_medium` mediumtext,
  `path_small` mediumtext,
  `path_thumbnails` text,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_app
-- ----------------------------
DROP TABLE IF EXISTS `pg_app`;
CREATE TABLE `pg_app` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID KDG App',
  `app_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Application Name',
  `app_author` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Application Author',
  `app_key` varchar(192) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Application Key',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Daftar Client APP yang terdaftar untuk bisa akses api / services.';

-- ----------------------------
-- Table structure for pg_category_complaint
-- ----------------------------
DROP TABLE IF EXISTS `pg_category_complaint`;
CREATE TABLE `pg_category_complaint` (
  `id` varchar(60) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_complaint
-- ----------------------------
DROP TABLE IF EXISTS `pg_complaint`;
CREATE TABLE `pg_complaint` (
  `id` varchar(60) NOT NULL,
  `photo_complaint` varchar(255) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `id_category` varchar(60) DEFAULT NULL,
  `time_created` int(11) DEFAULT NULL,
  `time_proceed` int(11) DEFAULT NULL,
  `photo_proceed` varchar(255) DEFAULT NULL,
  `notes_proceed` text,
  `time_solved` int(11) DEFAULT NULL,
  `photo_solved` varchar(255) DEFAULT NULL,
  `notes_solved` text,
  `status_process` tinyint(4) DEFAULT NULL,
  `status_respons_unit` tinyint(4) NOT NULL,
  `notes_mission` text,
  `tag` text,
  `source` tinyint(9) DEFAULT NULL,
  `description` text,
  `name_informer` varchar(100) DEFAULT NULL,
  `id_fk` varchar(200) DEFAULT NULL,
  `status_priority` tinyint(4) DEFAULT NULL,
  `id_phl` varchar(60) DEFAULT NULL,
  `time_reported` int(11) DEFAULT NULL,
  `id_division` varchar(60) DEFAULT NULL,
  `id_regency` varchar(15) DEFAULT NULL,
  `id_sub_district` varchar(15) DEFAULT NULL,
  `photo_complaint_social` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_contact
-- ----------------------------
DROP TABLE IF EXISTS `pg_contact`;
CREATE TABLE `pg_contact` (
  `id` varchar(60) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_division
-- ----------------------------
DROP TABLE IF EXISTS `pg_division`;
CREATE TABLE `pg_division` (
  `id` varchar(60) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_group_contact
-- ----------------------------
DROP TABLE IF EXISTS `pg_group_contact`;
CREATE TABLE `pg_group_contact` (
  `id` varchar(60) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_mst_kelurahan
-- ----------------------------
DROP TABLE IF EXISTS `pg_mst_kelurahan`;
CREATE TABLE `pg_mst_kelurahan` (
  `id` bigint(10) NOT NULL,
  `nama` varchar(50) CHARACTER SET latin1 NOT NULL,
  `id_sub_district` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_sub_district` (`id_sub_district`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pg_mst_regency
-- ----------------------------
DROP TABLE IF EXISTS `pg_mst_regency`;
CREATE TABLE `pg_mst_regency` (
  `id` varchar(15) NOT NULL,
  `id_province` varchar(15) NOT NULL,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_province` (`id_province`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_mst_sub_district
-- ----------------------------
DROP TABLE IF EXISTS `pg_mst_sub_district`;
CREATE TABLE `pg_mst_sub_district` (
  `id` varchar(15) NOT NULL,
  `id_regency` varchar(15) NOT NULL,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_regency` (`id_regency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_phl
-- ----------------------------
DROP TABLE IF EXISTS `pg_phl`;
CREATE TABLE `pg_phl` (
  `id` varchar(60) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_rel_contact_group
-- ----------------------------
DROP TABLE IF EXISTS `pg_rel_contact_group`;
CREATE TABLE `pg_rel_contact_group` (
  `id` varchar(60) NOT NULL,
  `id_contact` varchar(60) DEFAULT NULL,
  `id_group` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_contact` (`id_contact`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_session_token
-- ----------------------------
DROP TABLE IF EXISTS `pg_session_token`;
CREATE TABLE `pg_session_token` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID Session Token',
  `id_app` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID Registered Client App',
  `id_member` varchar(60) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ID Logged In Member',
  `session_key` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Session Key for Logged Session',
  `device_id` varchar(192) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Device ID (Google: GCM_ID; iOS: iOS_Push_Notif)',
  `os_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'OS Client Type (Android 4.1, Android 5.0, iOS 8.1, dll)',
  `created_time` datetime NOT NULL COMMENT 'created time',
  `session_log` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Session Token; Simpan semua session token untuk akses data via api / services;';

-- ----------------------------
-- Table structure for pg_source
-- ----------------------------
DROP TABLE IF EXISTS `pg_source`;
CREATE TABLE `pg_source` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `value` int(10) DEFAULT NULL,
  `text` varchar(100) DEFAULT NULL,
  `img` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_user_division
-- ----------------------------
DROP TABLE IF EXISTS `pg_user_division`;
CREATE TABLE `pg_user_division` (
  `id` varchar(60) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `id_division` varchar(100) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for pg_variable
-- ----------------------------
DROP TABLE IF EXISTS `pg_variable`;
CREATE TABLE `pg_variable` (
  `id` varchar(60) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sq_btq_group
-- ----------------------------
DROP TABLE IF EXISTS `sq_btq_group`;
CREATE TABLE `sq_btq_group` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_peserta` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_mentor` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `path_original` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_large` text COLLATE utf8_unicode_ci,
  `path_medium` mediumtext COLLATE utf8_unicode_ci,
  `path_small` mediumtext COLLATE utf8_unicode_ci,
  `path_thumbnails` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_btq_mentor
-- ----------------------------
DROP TABLE IF EXISTS `sq_btq_mentor`;
CREATE TABLE `sq_btq_mentor` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_customer` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hobi` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pendidikan` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jurusan` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peminatan` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_btq_peserta
-- ----------------------------
DROP TABLE IF EXISTS `sq_btq_peserta`;
CREATE TABLE `sq_btq_peserta` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_customer` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_customer
-- ----------------------------
DROP TABLE IF EXISTS `sq_customer`;
CREATE TABLE `sq_customer` (
  `id` varchar(60) NOT NULL,
  `id_sekolah` varchar(60) DEFAULT NULL,
  `id_mr` varchar(60) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `jns_kelamin` tinyint(4) DEFAULT NULL,
  `gol_darah` tinyint(4) DEFAULT NULL,
  `skill_khusus` varchar(200) DEFAULT NULL,
  `tempat_lahir` varchar(60) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `pekerjaan` varchar(60) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `sts_nikah` tinyint(4) DEFAULT NULL,
  `alamat` text,
  `is_membina` tinyint(4) DEFAULT NULL,
  `path_original` varchar(255) DEFAULT NULL,
  `path_large` text,
  `path_medium` mediumtext,
  `path_small` mediumtext,
  `path_thumbnails` text,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_penugasan` tinyint(4) DEFAULT '0',
  `ket` varchar(200) DEFAULT NULL,
  `path_file` mediumtext,
  `tipe` tinyint(4) DEFAULT NULL,
  `jabatan1` tinyint(4) DEFAULT NULL,
  `jabatan2` tinyint(4) DEFAULT NULL,
  `jabatan3` tinyint(4) DEFAULT NULL,
  `jabatan4` tinyint(4) DEFAULT NULL,
  `jabatan5` tinyint(4) DEFAULT NULL,
  `sts_aktif` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sq_gallery
-- ----------------------------
DROP TABLE IF EXISTS `sq_gallery`;
CREATE TABLE `sq_gallery` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_group` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitle` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deskripsi` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_original` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_large` text COLLATE utf8_unicode_ci,
  `path_medium` mediumtext COLLATE utf8_unicode_ci,
  `path_small` mediumtext COLLATE utf8_unicode_ci,
  `path_thumbnails` text COLLATE utf8_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_jurnal
-- ----------------------------
DROP TABLE IF EXISTS `sq_jurnal`;
CREATE TABLE `sq_jurnal` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_kegiatan` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipe_kegiatan` tinyint(4) DEFAULT NULL,
  `lokasi` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_tim` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_pimpinan` tinyint(4) DEFAULT NULL,
  `is_pimpinan_hadir` tinyint(4) DEFAULT NULL,
  `tgl_kegiatan` datetime DEFAULT NULL,
  `nama` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jml_peserta` int(11) DEFAULT NULL,
  `catatan` text COLLATE utf8_unicode_ci,
  `path_original` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_large` text COLLATE utf8_unicode_ci,
  `path_medium` mediumtext COLLATE utf8_unicode_ci,
  `path_small` mediumtext COLLATE utf8_unicode_ci,
  `path_thumbnails` text COLLATE utf8_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_mst_division
-- ----------------------------
DROP TABLE IF EXISTS `sq_mst_division`;
CREATE TABLE `sq_mst_division` (
  `id` varchar(60) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sq_mst_group
-- ----------------------------
DROP TABLE IF EXISTS `sq_mst_group`;
CREATE TABLE `sq_mst_group` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_mst_kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `sq_mst_kegiatan`;
CREATE TABLE `sq_mst_kegiatan` (
  `id` varchar(60) CHARACTER SET utf8 NOT NULL,
  `nama` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `sub_judul` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `tipe_pin` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `notes` text,
  `deskripsi` text,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sq_mst_sekolah
-- ----------------------------
DROP TABLE IF EXISTS `sq_mst_sekolah`;
CREATE TABLE `sq_mst_sekolah` (
  `id` varchar(60) NOT NULL,
  `nm_sekolah` varchar(200) DEFAULT NULL,
  `sts_lahan_dkw` tinyint(4) DEFAULT NULL COMMENT '0 = TIDAK\r\n            1 = YA',
  `sts_aktif_kelola` tinyint(4) DEFAULT NULL COMMENT '0 = NON AKTIF\r\n            1 = AKTIF\r\n            2 = PROSPEKTIF',
  `alamat` varchar(255) DEFAULT NULL,
  `thn_gabung` varchar(4) DEFAULT NULL,
  `path_original` varchar(255) DEFAULT NULL,
  `path_large` text,
  `path_medium` mediumtext,
  `path_small` mediumtext,
  `path_thumbnails` text,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sq_notifikasi
-- ----------------------------
DROP TABLE IF EXISTS `sq_notifikasi`;
CREATE TABLE `sq_notifikasi` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_member_creator` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_member_tujuan` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_data` smallint(6) DEFAULT '1',
  `type_action` smallint(6) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_tim
-- ----------------------------
DROP TABLE IF EXISTS `sq_tim`;
CREATE TABLE `sq_tim` (
  `id` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `id_pimpinan` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_bentuk` date DEFAULT NULL,
  `tgl_berakhir` date DEFAULT NULL,
  `ket` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_original` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_large` text COLLATE utf8_unicode_ci,
  `path_medium` mediumtext COLLATE utf8_unicode_ci,
  `path_small` mediumtext COLLATE utf8_unicode_ci,
  `path_thumbnails` text COLLATE utf8_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `tipe` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for sq_tim_anggota
-- ----------------------------
DROP TABLE IF EXISTS `sq_tim_anggota`;
CREATE TABLE `sq_tim_anggota` (
  `id` varchar(60) CHARACTER SET utf8 NOT NULL,
  `id_anggota` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `nama_anggota` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `id_tim` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `nama_tim` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `alamat` text,
  `ket` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `path_original` varchar(255) DEFAULT NULL,
  `path_large` text,
  `path_medium` mediumtext,
  `path_small` mediumtext,
  `path_thumbnails` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for view_pg_rel_contact_group
-- ----------------------------
DROP TABLE IF EXISTS `view_pg_rel_contact_group`;
CREATE TABLE `view_pg_rel_contact_group` (
  `id` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_contact` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_group` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_group` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_contact` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
