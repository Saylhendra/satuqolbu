drop table if exists sq_btq_peserta;

/*==============================================================*/
/* Table: sq_btq_peserta                                        */
/*==============================================================*/
create table sq_btq_peserta
(
   id                   varchar(60) not null,
   id_customer          varchar(60),
   nama                 varchar(200),
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);