drop table if exists sq_notifikasi;

/*==============================================================*/
/* Table: sq_notifikasi                                         */
/*==============================================================*/
create table sq_notifikasi
(
   id                   varchar(60) not null,
   id_member_creator    varchar(60),
   id_member_tujuan     varchar(60),
   type_data            smallint(6) default 1,
   type_action          smallint(6),
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);

drop table if exists sq_gallery;

/*==============================================================*/
/* Table: sq_gallery                                            */
/*==============================================================*/
create table sq_gallery
(
   id                   varchar(60) not null,
   id_group             varchar(60),
   nama                 varchar(200),
   subtitle             varchar(200),
   deskripsi            varchar(200),
   path_original        varchar(255),
   path_large           text,
   path_medium          mediumtext,
   path_small           mediumtext,
   path_thumbnails      text,
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);

drop table if exists sq_mst_group;

/*==============================================================*/
/* Table: sq_mst_group                                          */
/*==============================================================*/
create table sq_mst_group
(
   id                   varchar(60) not null,
   nama                 varchar(200),
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);
