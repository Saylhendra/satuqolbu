drop table if exists sq_mst_kegiatan;

/*==============================================================*/
/* Table: sq_mst_kegiatan                                       */
/*==============================================================*/
create table sq_mst_kegiatan
(
   id                   varchar(60) not null,
   nama                 varchar(100),
   notes                text,
   deskripsi            text,
   primary key (id)
);


drop table if exists sq_tim;

/*==============================================================*/
/* Table: sq_tim                                                */
/*==============================================================*/
create table sq_tim
(
   id                   varchar(60) not null,
   id_pimpinan          varchar(60),
   nama                 varchar(200),
   tgl_bentuk           datetime,
   path_original        varchar(255),
   path_large           text,
   path_medium          mediumtext,
   path_small           mediumtext,
   path_thumbnails      text,
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);

drop table if exists sq_tim_anggota;

/*==============================================================*/
/* Table: sq_tim_anggota                                        */
/*==============================================================*/
create table sq_tim_anggota
(
   id                   varchar(60) not null,
   id_anggota           varchar(60),
   id_tim               varchar(60),
   created_date         datetime,
   update_date          datetime,
   primary key (id)
);

drop table if exists sq_jurnal;

/*==============================================================*/
/* Table: sq_jurnal                                             */
/*==============================================================*/
create table sq_jurnal
(
   id                   varchar(60) not null,
   id_kegiatan     varchar(60),
   id_tim               varchar(60),
   tgl_kegiatan         datetime,
   nama                 varchar(60),
   jml_peserta          int,
   catatan              text,
   path_original        varchar(255),
   path_large           text,
   path_medium          mediumtext,
   path_small           mediumtext,
   path_thumbnails      text,
   created_date         datetime,
   update_date          datetime,
   status               tinyint(4) default 0,
   primary key (id)
);
